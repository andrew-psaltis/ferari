package eu.ferari.backend.storm.spouts;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import eu.ferari.core.utils.Check;
import eu.ferari.core.utils.CheckFailedException;
import eu.ferari.core.utils.Helper;

/**
 * Spout implementation to read from a single file. Does not handle any
 * re-reading of a failed tuple in topology.
 */
public class FileLineReaderSpout extends BaseRichSpout {
    private static final String RESULT_FIELD_NAME = "word";
    private static final long serialVersionUID = 1L;
    SpoutOutputCollector _collector;
    private BufferedReader in;

    @Override
    public void open(@SuppressWarnings("rawtypes") Map conf, TopologyContext context, SpoutOutputCollector collector) {
        _collector = collector;

        File inFile = new File((String) conf.get("INFILE"));
        try {
            this.in = new BufferedReader(new FileReader(inFile));
        } catch (IOException e) {
            throw new CheckFailedException("Cant open:" + inFile + Helper.stackTraceOf(e));
        }

    }

    @Override
    public void nextTuple() {
        Check.notNull(in);

        try {
            String line = in.readLine();
            if (line != null) {
                _collector.emit(new Values(line));
            }
        } catch (IOException e) {
            throw new CheckFailedException("Cant read from:" + in + Helper.stackTraceOf(e));
        }

    }

    @Override
    public void close() {
        try {
            in.close();
            in = null;
        } catch (IOException e) {
            throw new CheckFailedException("Cant close:" + in + Helper.stackTraceOf(e));
        }

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields(RESULT_FIELD_NAME));
    }

}