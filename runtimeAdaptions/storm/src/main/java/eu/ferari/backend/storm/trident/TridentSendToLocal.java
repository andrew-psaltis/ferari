package eu.ferari.backend.storm.trident;

import storm.trident.operation.TridentCollector;
import backtype.storm.tuple.Values;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

public class TridentSendToLocal implements ISend {

    private final TridentCollector collector;

    public TridentSendToLocal(TridentCollector collector) {
        this.collector = collector;
    }

    @Override
    public void signal(DataTuple data) {
        Values values = new Values();
        values.add(data);
        collector.emit(values);

    }

}
