package eu.ferari.backend.storm.bolts;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;

import javax.imageio.IIOException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * Created by mofu on 3/22/15.
 */
public class FileWriterBolt extends BaseRichBolt implements Serializable{

    public static final long serialVersionUID= 1l;
    private String filePath;
    private int numEntries;
    OutputCollector _collector;

    public FileWriterBolt(String filePath, int numEntries) {
        this.numEntries=numEntries;
            this.filePath = filePath;
    }

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this._collector=collector;
    }

    public void execute(Tuple tuple){
        FileWriter writer = null;
        try {
            writer = new FileWriter(filePath);
        } catch (IOException e) {
            return;
        }
        StringBuffer sb = new StringBuffer();
        eu.ferari.core.DataTuple ftuple=(eu.ferari.core.DataTuple)tuple.getValue(0);
        for(int i =0;i<numEntries;i++){
            sb.append(ftuple.getValue(0).toString());
            sb.append('\t');
        }
        sb.setCharAt(sb.length()-1,'\n');
        try {
            writer.write(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            writer.close();
        } catch (IOException e) {
            return;
        }
        _collector.ack(tuple);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }
}
