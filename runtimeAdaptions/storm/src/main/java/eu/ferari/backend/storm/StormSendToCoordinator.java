package eu.ferari.backend.storm;

import backtype.storm.task.OutputCollector;
import backtype.storm.tuple.Values;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

public class StormSendToCoordinator implements ISend {

    public static final String TO_COORDINATOR_STREAM_NAME = "sendToCoordinator";

    public static final String FIELDNAME = "data";

    private final OutputCollector collector;

    public StormSendToCoordinator(OutputCollector collector) {
        this.collector = collector;
    }

    @Override
    public void signal(DataTuple data) {
        Values values = new Values();
        values.add(data);
        this.collector.emit(TO_COORDINATOR_STREAM_NAME, values);

    }

}
