package eu.ferari.backend.storm.bolts;

import java.util.Map;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ICoordinatorState;

/**
 * Bolt to host the Coordinator.
 * <p>
 * This bolt represents the Coordinator and holds the state associated with it.
 */
public class CoordinatorBolt extends BaseRichBolt {
    private static final long serialVersionUID = 1L;
    OutputCollector collector;
    private final ICoordinatorState state;

    public CoordinatorBolt(ICoordinatorState state) {
        super();
        this.state = state;
    }

    @Override
    public void prepare(@SuppressWarnings("rawtypes") Map conf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        state.setSender(new StormSendToLocal(collector));

    }

    @Override
    public void execute(Tuple tuple) {

        if (tuple.getSourceStreamId().equals(StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME)) {
            state.update((DataTuple) tuple.getValue(0));
        }

        collector.ack(tuple);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declareStream(StormSendToLocal.TO_LOCAL_STREAM_NAME, new Fields(StormSendToLocal.FIELDNAME));
    }

}
