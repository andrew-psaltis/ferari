/**
 * 
 */
package eu.ferari.examples.distributedcount.states;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.distributedcount.misc.Message;
import eu.ferari.examples.distributedcount.misc.Message.MessageType;
import eu.ferari.examples.distributedcount.states.GatekeeperState;

import org.junit.Before;
import org.junit.Test;


/**
 * @author yoni
 *
 */
public class GatekeeperStateTest {
	private GatekeeperState gatekeeperState;
	private ISend mockSendToCommunicator;
	
	private String nodeId = "node1";
	private String phone = "123";
	private long timestamp = 1l;
	
	@Before
	public void setUp() throws Exception {
		mockSendToCommunicator = mock(ISend.class);
		gatekeeperState = new GatekeeperState(mockSendToCommunicator, 5);
	}

	@Test
	public void testUpdateThreshold_UpdatesThreshold() {
		int newThreshold = gatekeeperState.getDefaultThreshold() - 1;
		Message newThresholdMessage = new Message(MessageType.NewThreshold, nodeId, phone, newThreshold);
		
		gatekeeperState.update(newThresholdMessage.toDataTuple());
		
		assertThat(gatekeeperState.getThreshold(phone), equalTo(newThreshold));
	}		

	@Test
	public void testMessageBelowThreshold_DoesNotPass() {
		int counter = gatekeeperState.getDefaultThreshold() - 1;
		Message counterUpdateMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		
		gatekeeperState.update(counterUpdateMessage.toDataTuple());
		
		verifyZeroInteractions(mockSendToCommunicator);
	}
	
	@Test
	public void testMessageAboveThreshold_Passes() {
		int counter = gatekeeperState.getDefaultThreshold() + 1;
		Message counterUpdateMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		
		gatekeeperState.update(counterUpdateMessage.toDataTuple());
		
		verify(mockSendToCommunicator, times(1)).signal(counterUpdateMessage.toDataTuple());
	}
}
