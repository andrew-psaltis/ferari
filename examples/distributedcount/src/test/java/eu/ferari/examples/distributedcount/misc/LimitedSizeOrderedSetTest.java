package eu.ferari.examples.distributedcount.misc;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.util.NavigableSet;

import org.junit.Before;
import org.junit.Test;

public class LimitedSizeOrderedSetTest {
	private final int MAX_SIZE = 3;
	private LimitedSizeOrderedSet<Integer> set;
	
	@Before
	public void setUp() throws Exception {
		set = new LimitedSizeOrderedSet<Integer>(MAX_SIZE);
		set.add(1);
		set.add(3);
		set.add(5);
	}

	@Test
	public void testAddElementAtFullCapacity_PopsFirstElement() {
		set.add(7);
		assertThat(set.size(), is(equalTo(MAX_SIZE)));
		assertThat(set.first(), is(equalTo(3)));		
	}
	
	@Test
	public void testRewindGetsCorrectSubset_WhenElementExists() {
		NavigableSet<Integer> rewindResult = set.rewind(3);
		assertThat(rewindResult.size(), is(equalTo(2)));
		assertTrue(rewindResult.contains(3));
		assertTrue(rewindResult.contains(5));
		
	}
	
	@Test
	public void testRewindGetsCorrectSubset_WhenElementDoesNotExist_AndLessThanExistingMaximum() {
		NavigableSet<Integer> rewindResult = set.rewind(4);
		assertThat(rewindResult.size(), is(equalTo(1)));
		assertTrue(rewindResult.contains(5));		
	}
	
	@Test
	public void testRewindGetsCorrectSubset_WhenElementDoesNotExist() {
		NavigableSet<Integer> rewindResult = set.rewind(6);
		assertThat(rewindResult.size(), is(equalTo(0)));
	}
	
	@Test
	public void testGetGetsCorrectElement(){
		int result = set.get(3);
		assertThat(result, is(equalTo(3)));
	}
	
	@Test
	public void testGetGetsNull_IfDoesNotExist(){
		Integer result = set.get(7);
		assertNull(result);
	}
}
