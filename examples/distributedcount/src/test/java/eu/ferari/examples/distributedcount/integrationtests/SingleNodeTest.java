package eu.ferari.examples.distributedcount.integrationtests;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;

import org.junit.Ignore;
import org.junit.Test;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPServiceDestroyedException;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.deploy.DeploymentException;
import com.espertech.esper.client.deploy.ParseException;
import com.google.gson.Gson;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.topology.BoltDeclarer;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseRichSpout;
import eu.ferari.examples.distributedcount.misc.CdrEvent;
import eu.ferari.examples.distributedcount.misc.JedisConfiguration;
import eu.ferari.examples.distributedcount.misc.Message;
import eu.ferari.examples.distributedcount.misc.RedisListenerThread;
import eu.ferari.examples.distributedcount.misc.Utils;
import eu.ferari.examples.distributedcount.storm.DistributedCountTopology;
import eu.ferari.examples.distributedcount.storm.bolts.CommunicatorBolt;
import eu.ferari.examples.distributedcount.storm.bolts.EsperBolt;
import eu.ferari.examples.distributedcount.storm.bolts.GatekeeperBolt;
import eu.ferari.examples.distributedcount.storm.bolts.TimeMachineBolt;
import eu.ferari.examples.distributedcount.storm.spouts.FileSpout;

public class SingleNodeTest {		
	
	@Test
	public void testOpaque() throws IOException {
		Properties properties = Utils.loadProperties("topology.properties", this);
		String commToCoordinatorChannel = properties.getProperty("commToCoordinatorChannel");
		
		final JedisPoolConfig poolConfig = new JedisPoolConfig();
		final JedisConfiguration jedisConfig = new JedisConfiguration();
		final JedisPool jedisPool = new JedisPool(poolConfig, jedisConfig.getHost(),
				jedisConfig.getPort(), jedisConfig.getTimeout());
		
		LinkedBlockingQueue<String> messageQueue = new LinkedBlockingQueue<String>(5);
        RedisListenerThread redisListener = new RedisListenerThread(messageQueue, jedisPool, commToCoordinatorChannel);
        redisListener.start();
        
        DistributedCountTopology topology = new DistributedCountTopology("topology.properties", "singleNodeEvents.txt");
		topology.start();
        
        backtype.storm.utils.Utils.sleep(10000);
        topology.stop();
        redisListener.quit();        
        jedisPool.close();
        jedisPool.destroy();

        assertThat(messageQueue.size(), is(equalTo(2)));
        Gson gson = new Gson();
        for (Iterator<String> iterator = messageQueue.iterator(); iterator.hasNext();){
        	String json = iterator.next();
        	if (json == null)
        		continue;
            Message message = gson.fromJson(json, Message.class);
            switch (message.getPhone()) {
			case "1":
				assertThat(message.getCounter(), is(equalTo(2)));
				break;
			case "2":
				assertThat(message.getCounter(), is(equalTo(2)));
				break;
			default:
				fail("Unknown phone. Message: " + message);
			}
        }   
	}
}
