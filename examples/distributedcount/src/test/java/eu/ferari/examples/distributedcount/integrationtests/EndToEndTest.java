package eu.ferari.examples.distributedcount.integrationtests;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;

import org.junit.Test;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;

import backtype.storm.utils.Utils;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.distributedcount.misc.CdrEvent;
import eu.ferari.examples.distributedcount.misc.Coordinator;
import eu.ferari.examples.distributedcount.storm.DistributedCountTopology;

public class EndToEndTest {	
	
	@Test
	public void test() throws Exception {		
        ISend mockGlobalViolationReporter = mock(ISend.class);
		DistributedCountTopology topology1 = new DistributedCountTopology("endToEnd/topology-a11.properties", "endToEnd/events-a11.txt");
		DistributedCountTopology topology2 = new DistributedCountTopology("endToEnd/topology-a12.properties", "endToEnd/events-a12.txt");
		Coordinator coordinator = new Coordinator("endToEnd/topology-a11.properties", mockGlobalViolationReporter);
		coordinator.start();
		topology1.start();
		topology2.start();
		Utils.sleep(10000);
		coordinator.stop();
		topology1.stop();
		topology2.stop();
		
		String phone = "1";
		long timestamp = 0;
		int count = 6;
		DataTuple tuple = new DataTuple(phone, timestamp, count);
		verify(mockGlobalViolationReporter, times(1)).signal(tuple);
	}

}
