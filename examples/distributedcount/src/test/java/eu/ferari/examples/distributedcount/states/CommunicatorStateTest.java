package eu.ferari.examples.distributedcount.states;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import org.junit.Before;
import org.junit.Test;

import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.distributedcount.misc.Message;
import eu.ferari.examples.distributedcount.misc.Message.MessageType;
import eu.ferari.examples.distributedcount.states.CommunicatorState.State;

public class CommunicatorStateTest {

	private ISend mockSendToTimeMachine;	
	private ISend mockSendToGatekeeper;
	private ISend mockSendToCoordinator;
	private CommunicatorState communicatorState;
	
	private String nodeId = "node1";
	private String phone = "123";
	private long timestamp = 1l;
	
	@Before
	public void setUp() throws Exception {
		mockSendToTimeMachine = mock(ISend.class);
		mockSendToGatekeeper = mock(ISend.class);
		mockSendToCoordinator = mock(ISend.class);
		communicatorState = 
				new CommunicatorState(mockSendToTimeMachine, mockSendToGatekeeper, mockSendToCoordinator);
	}

	@Test
	public void testLocalThresholdCrossing__WhenMonitoring_SetsTimeAndState_SendsPause() {
		Message counterUpdateMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, 1);
		Message pauseMessage = new Message(MessageType.Pause, nodeId, phone, timestamp);
		
		communicatorState.update(counterUpdateMessage.toDataTuple());
		
		assertThat(communicatorState.getState(phone), is(equalTo(State.PausingLocalThresholdCrossing)));
		assertThat(communicatorState.getRegisteredTime(phone), is(equalTo(timestamp)));
		verify(mockSendToTimeMachine, times(1)).signal(pauseMessage.toDataTuple());
	}
	
	/**
	 * Test local threshold crossing when in Pausing - Counter Request Message state
	 * and message timestamp less than existing timestamp, updates timestamp and state
	 */
	@Test
	public void testThresholdCrossing__WhenPausingCR_EarlierTimestamp_SetsTimeAndState() {
		communicatorState.setStateAndTime(phone, State.PausingCounterRequestMessage, timestamp);
		long newTimestamp = timestamp - 1;
		Message counterUpdateMessage = new Message(MessageType.CounterUpdate, nodeId, phone, newTimestamp, 1);
		
		communicatorState.update(counterUpdateMessage.toDataTuple());
		
		assertThat(communicatorState.getState(phone), is(equalTo(State.PausingLocalThresholdCrossing)));
		assertThat(communicatorState.getRegisteredTime(phone), is(equalTo(newTimestamp)));
	}
	
	/**
	 * Test local threshold crossing when in Pausing - Counter Request Message state
	 * and message timestamp same as existing timestamp, does nothing.
	 */
	@Test
	public void testThresholdCrossing__WhenPausingCR_LaterTimestamp_DoesNothing() {
		communicatorState.setStateAndTime(phone, State.PausingCounterRequestMessage, timestamp);
		Message counterUpdateMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, 1);
		
		communicatorState.update(counterUpdateMessage.toDataTuple());
		
		assertThat(communicatorState.getState(phone), is(equalTo(State.PausingCounterRequestMessage)));
		assertThat(communicatorState.getRegisteredTime(phone), is(equalTo(timestamp)));
	}
	
	/**
	 * Test local threshold crossing when in Pausing - Local Threshold Crossing does
	 * nothing.
	 */
	@Test
	public void testThresholdCrossing__WhenPausingLTC_DoesNothing() {
		communicatorState.setStateAndTime(phone, State.PausingLocalThresholdCrossing, timestamp);
		long newTimestamp = timestamp - 1;
		Message counterUpdateMessage = new Message(MessageType.CounterUpdate, nodeId, phone, newTimestamp, 1);
		
		communicatorState.update(counterUpdateMessage.toDataTuple());
		
		assertThat(communicatorState.getState(phone), is(equalTo(State.PausingLocalThresholdCrossing)));
		assertThat(communicatorState.getRegisteredTime(phone), is(equalTo(timestamp)));
	}
	
	@Test
	public void testPauseConfirmation_WhenPausingCR_SetsState_SendsCounterReply(){
		int counter = 1;
		communicatorState.setStateAndTime(phone, State.PausingCounterRequestMessage, timestamp);
		Message pauseConfirmationMessage = new Message(MessageType.PauseConfirmation, nodeId, phone, timestamp, counter);
		Message counterReplyMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		
		communicatorState.update(pauseConfirmationMessage.toDataTuple());
		
		assertThat(communicatorState.getState(phone), is(equalTo(State.WaitingForCoordinator)));
		verify(mockSendToCoordinator, times(1)).signal(counterReplyMessage.toDataTuple());
	}
	
	@Test
	public void testPauseConfirmation_WhenPausingLTC_SetsState_SendsCounter(){
		int counter = 1;
		communicatorState.setStateAndTime(phone, State.PausingLocalThresholdCrossing, timestamp);
		Message pauseConfirmationMessage = new Message(MessageType.PauseConfirmation, nodeId, phone, timestamp, counter);
		Message counterUpdateMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		
		communicatorState.update(pauseConfirmationMessage.toDataTuple());
		
		assertThat(communicatorState.getState(phone), is(equalTo(State.WaitingForCoordinator)));
		verify(mockSendToCoordinator, times(1)).signal(counterUpdateMessage.toDataTuple());
	}
	
	@Test
	public void testCounterRequest_WhenMonitoring_SetsTimeAndState_SendsPause(){
		String fakeNodeId = nodeId + "a";
		Message counterRequestMessage = new Message(MessageType.CounterRequest, nodeId, phone, timestamp);
		Message pauseMessage = new Message(MessageType.Pause, fakeNodeId, phone, timestamp);
		communicatorState.setNodeId(fakeNodeId);
		
		communicatorState.update(counterRequestMessage.toDataTuple());
		
		assertThat(communicatorState.getState(phone), is(equalTo(State.PausingCounterRequestMessage)));
		assertThat(communicatorState.getRegisteredTime(phone), is(equalTo(timestamp)));
		verify(mockSendToTimeMachine, times(1)).signal(pauseMessage.toDataTuple());
	}
	
	@Test
	public void testCounterRequest_WhenPausingCR_EarlierTimestamp_SetsTime() {
		communicatorState.setStateAndTime(phone, State.PausingCounterRequestMessage, timestamp);
		String fakeNodeId = nodeId + "f";
		communicatorState.setNodeId(fakeNodeId);
		long newTimestamp = timestamp - 1;
		Message counterRequestMessage = new Message(MessageType.CounterRequest, nodeId, phone, newTimestamp);
		
		communicatorState.update(counterRequestMessage.toDataTuple());
		
		assertThat(communicatorState.getState(phone), is(equalTo(State.PausingCounterRequestMessage)));
		assertThat(communicatorState.getRegisteredTime(phone), is(equalTo(newTimestamp)));
	}
	
	@Test
	public void testCounterRequest_WhenPausingLTC_EarlierTimestamp_SetsTimeAndState() {
		communicatorState.setStateAndTime(phone, State.PausingLocalThresholdCrossing, timestamp);
		String fakeNodeId = nodeId + "f";
		communicatorState.setNodeId(fakeNodeId);
		long newTimestamp = timestamp - 1;
		Message counterRequestMessage = new Message(MessageType.CounterRequest, nodeId, phone, newTimestamp);
		
		communicatorState.update(counterRequestMessage.toDataTuple());
		
		assertThat(communicatorState.getState(phone), is(equalTo(State.PausingCounterRequestMessage)));
		assertThat(communicatorState.getRegisteredTime(phone), is(equalTo(newTimestamp)));
	}
	
	@Test
	public void testCounterRequest_WhenWaitingForCoordinator_EarlierTimestamp_SetsTimeAndState_SendsPause() {
		communicatorState.setStateAndTime(phone, State.WaitingForCoordinator, timestamp);
		String fakeNodeId = nodeId + "f";
		communicatorState.setNodeId(fakeNodeId);
		long newTimestamp = timestamp - 1;
		Message counterRequestMessage = new Message(MessageType.CounterRequest, nodeId, phone, newTimestamp);
		Message pauseMessage = new Message(MessageType.Pause, fakeNodeId, phone, newTimestamp);
		
		communicatorState.update(counterRequestMessage.toDataTuple());
		
		assertThat(communicatorState.getState(phone), is(equalTo(State.PausingCounterRequestMessage)));
		assertThat(communicatorState.getRegisteredTime(phone), is(equalTo(newTimestamp)));
		verify(mockSendToTimeMachine, times(1)).signal(pauseMessage.toDataTuple());
	}
	
	@Test
	public void testUpdateThreshold_WhenWaitingForCoordinator_SendsPauseAndNewThreshold(){
		communicatorState.setStateAndTime(phone, State.WaitingForCoordinator, timestamp);
		communicatorState.setNodeId(nodeId);
		int newThreshold = 10;
		Message updateThresholdMessage = new Message(MessageType.UpdateThreshold, nodeId, phone, timestamp, newThreshold);
		Message newThresholdMessage = new Message(MessageType.NewThreshold, nodeId, phone, newThreshold);
		Message playMessage = new Message(MessageType.Play, nodeId, phone, timestamp);		
		
		communicatorState.update(updateThresholdMessage.toDataTuple());
		
		assertThat(communicatorState.getState(phone), is(equalTo(State.Monitoring)));
		assertThat(communicatorState.getRegisteredTime(phone), is(equalTo(-1l)));
		verify(mockSendToTimeMachine, times(1)).signal(playMessage.toDataTuple());
		verify(mockSendToGatekeeper, times(1)).signal(newThresholdMessage.toDataTuple());
	}
}
