/**
 * 
 */
package eu.ferari.examples.distributedcount.misc;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import org.junit.Test;

import eu.ferari.examples.distributedcount.exceptions.NodeDataAlreadyExistsException;
import eu.ferari.examples.distributedcount.exceptions.NodeDataNotYetCompleteException;
import eu.ferari.examples.distributedcount.exceptions.NodeListCompleteException;

/**
 * @author yoni
 *
 */
public class ViolationTest {

	@Test
	public void testAddWithLowerTimestamp_ResetsList() throws NodeDataAlreadyExistsException, NodeListCompleteException {
		long timestamp = 1l;
		String node1 = "node1";
		String node2 = "node2";
		
		Violation violation = new Violation("123", 2, timestamp);
		violation.add(node1, timestamp, 1);
		violation.add(node2, timestamp - 1, 1);
		
		assertThat(violation.getNumReportedNodes(), is(equalTo(1)));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddWithHigherTimestamp_DoesNothing() throws NodeDataAlreadyExistsException, NodeListCompleteException {
		long timestamp = 1l;
		String node1 = "node1";
		String node2 = "node2";
		
		Violation violation = new Violation("123", 2, timestamp);
		violation.add(node1, timestamp, 1);
		violation.add(node2, timestamp + 1, 1);
		
		assertThat(violation.getNumReportedNodes(), is(equalTo(1)));
	}
	
	@Test
	public void testIncompleteNodeData_ReturnsFalse() throws NodeDataAlreadyExistsException, NodeListCompleteException {
		long timestamp = 1l;
		String node1 = "node1";
		
		Violation violation = new Violation("123", 2, timestamp);
		boolean isComplete = violation.add(node1, timestamp, 1);		
		
		assertThat(isComplete, is(false));
	}
	
	@Test
	public void testCompleteNodeData_ReturnsTrue() throws NodeDataAlreadyExistsException, NodeListCompleteException {
		long timestamp = 1l;
		String node1 = "node1";
		
		Violation violation = new Violation("123", 1, timestamp);
		boolean isComplete = violation.add(node1, timestamp, 1);		
		
		assertThat(isComplete, is(true));
	}
	
	@Test
	public void testGetNewThreshold_ReturnsCorrectValue() throws NodeDataAlreadyExistsException, NodeDataNotYetCompleteException, NodeListCompleteException {
		long timestamp = 1l;
		String node1 = "node1";
		int counter1 = 4;
		int counter2 = counter1 + 2;
		int numNodes = 2;
		
		Violation violation = new Violation("123", numNodes, timestamp);
		violation.add(node1, timestamp, counter1);
		violation.add("node2", timestamp, counter2);
		
		int newThreshold = violation.getNewThreshold(node1);
		int expectedThreshold = (Violation.DEFAULT_GLOBAL_THRESHOLD - (counter1 + counter2)) / numNodes + counter1;
		assertThat(newThreshold, is(equalTo(expectedThreshold)));
	}
	
	@Test
	public void testAddingNodeTwice_ThrowsException() throws NodeDataAlreadyExistsException, NodeListCompleteException {
		long timestamp = 1l;
		String node1 = "node1";
		
		Violation violation = new Violation("123", 2, timestamp);
		violation.add(node1, timestamp, 1);
		
		try {
			violation.add(node1, timestamp, 1);
			fail("Expected NodeDataAlreadyExistsException");
		} catch (NodeDataAlreadyExistsException e) {
		}
		
		assertThat(violation.getNumReportedNodes(), is(equalTo(1)));
	}
	
	@Test
	public void testGetNewThresholdWithoutAllNodes_ThrowsException(){
		Violation violation = new Violation("123", 2, 1l);
		try {
			violation.getNewThreshold("");
			fail("Expected NodeDataNotYetCompleteException");
		} catch (NodeDataNotYetCompleteException e) {
		}
	}
}
