package eu.ferari.examples.distributedcount.misc;

import static org.junit.Assert.*;

import org.junit.Test;
import static org.hamcrest.Matchers.*;

public class TimeWindowCountTest {

	@Test
	public void testWindowsEqual_WhenTimestampsEqual() {
		long timestamp = 1l;
		TimeWindowCount window1 = new TimeWindowCount(3, timestamp);
		TimeWindowCount window2 = new TimeWindowCount(4, timestamp);
		
		assertThat(window1, is(equalTo(window2)));
	}
	
	@Test
	public void testWindowsComparison_SameAsTimestampComparison() {
		long timestamp = 1l;
		TimeWindowCount window1 = new TimeWindowCount(3, timestamp);
		TimeWindowCount window2 = new TimeWindowCount(4, timestamp - 1);
		
		assertThat(1, is(equalTo(window1.compareTo(window2))));
		assertThat(-1, is(equalTo(window2.compareTo(window1))));
	}
}
