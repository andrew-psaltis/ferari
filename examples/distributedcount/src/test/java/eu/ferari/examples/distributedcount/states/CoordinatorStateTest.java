/**
 * 
 */
package eu.ferari.examples.distributedcount.states;

import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.distributedcount.misc.Message;
import eu.ferari.examples.distributedcount.misc.Message.MessageType;
import eu.ferari.examples.distributedcount.misc.Violation;

/**
 * @author yoni
 *
 */
public class CoordinatorStateTest {
	private ISend mockSendCounterRequest;
	private ISend mockSendThresholdUpdate;
	private Message thresholdPassMessage;
	private String nodeId;
	private String phone;
	private long timestamp;
	private int counter;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		mockSendCounterRequest = mock(ISend.class);
		mockSendThresholdUpdate = mock(ISend.class);
		nodeId = "node1";
		phone = "123";
		timestamp = 1l;
		counter = 4;
		
		thresholdPassMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
	}

	@Test
	public void testReceiveThresholdPassNodeListComplete_SendsNewThresholds() {
		int newThreshold = (Violation.DEFAULT_GLOBAL_THRESHOLD - counter) / 1 + counter;
		Message newThresholdMessage = 
				new Message(MessageType.UpdateThreshold, thresholdPassMessage.getNodeId(), thresholdPassMessage.getPhone(), 1l, newThreshold);
		CoordinatorState coordinatorState = new CoordinatorState(mockSendCounterRequest, mockSendThresholdUpdate, 1, Violation.DEFAULT_GLOBAL_THRESHOLD);
		coordinatorState.update(thresholdPassMessage.toDataTuple());
		
		verify(mockSendThresholdUpdate, times(1)).signal(newThresholdMessage.toDataTuple());		
	} // TODO: get rid of definition of global threshold in Violation class
	
	@Test
	public void testReceiveThresholdPassNodeListComplete_ReportsViolation() {		
		ISend mockViolationReporter = mock(ISend.class);
		CoordinatorState coordinatorState = new CoordinatorState(mockSendCounterRequest, mockSendThresholdUpdate, 1, Violation.DEFAULT_GLOBAL_THRESHOLD);
		coordinatorState.setViolationReporter(mockViolationReporter);
		DataTuple violationReport = new DataTuple(thresholdPassMessage.getPhone(), thresholdPassMessage.getTimestamp(), counter);
		coordinatorState.update(thresholdPassMessage.toDataTuple());
		
		verify(mockViolationReporter, times(1)).signal(violationReport);		
	}
	
	@Test
	public void testReceiveFirstViolation_SendsCounterRequests() {
		Message counterRequestMessage = new Message(MessageType.CounterRequest, nodeId, phone, timestamp);
		CoordinatorState coordinatorState = new CoordinatorState(mockSendCounterRequest, mockSendThresholdUpdate, 2, Violation.DEFAULT_GLOBAL_THRESHOLD);
		coordinatorState.update(thresholdPassMessage.toDataTuple());
		
		verify(mockSendCounterRequest, times(1)).signal(counterRequestMessage.toDataTuple());	
	}
	
	@Test
	public void testReceiveThresholdPassNodeListIncomplete_DoesNothing() {
		Message secondThresholdPassMessage = new Message(MessageType.CounterUpdate, "node2", phone, timestamp, counter);
		
		CoordinatorState coordinatorState = new CoordinatorState(mockSendCounterRequest, mockSendThresholdUpdate, 3, Violation.DEFAULT_GLOBAL_THRESHOLD);
		coordinatorState.update(thresholdPassMessage.toDataTuple());
		coordinatorState.update(secondThresholdPassMessage.toDataTuple());
		
		verify(mockSendCounterRequest, times(1)).signal(Mockito.any(DataTuple.class));
	}
}
