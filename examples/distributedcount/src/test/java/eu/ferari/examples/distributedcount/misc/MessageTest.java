package eu.ferari.examples.distributedcount.misc;

import static org.junit.Assert.*;

import org.junit.Test;

import com.google.gson.Gson;

import eu.ferari.examples.distributedcount.misc.Message.MessageType;

public class MessageTest {

	@Test
	public void testJSONSerialization() {
		String nodeId = "node1";
		String phone = "123";
		long timestamp = 1l;
		int count = 3;
		Message message = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, count);
		
		String json = message.toJSON();
		Gson gson = new Gson();
		Message mess = gson.fromJson(json, Message.class);
		assertTrue(true);
	}
	
	// TODO: write all tests
}
