package eu.ferari.examples.distributedcount.states;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.distributedcount.misc.Message;
import eu.ferari.examples.distributedcount.misc.Message.MessageType;
import eu.ferari.examples.distributedcount.states.TimeMachineState.PhoneState;

//TODO: examine all warnings
public class TimeMachineStateTest {
	private ISend mockSendToGatekeeper;
	private ISend mockSendToCommunicator;
	private TimeMachineState timeMachineState;
	private String nodeId;
	private String phone;
	private long timestamp;
	
	@Before
	public void setUp() throws Exception {
		mockSendToGatekeeper = mock(ISend.class);
		mockSendToCommunicator = mock(ISend.class);
		timeMachineState = new TimeMachineState(mockSendToGatekeeper, mockSendToCommunicator);
		nodeId = "node1";
		phone = "123";
		timestamp = 1l;
	}
	
	@Test
	public void testReceiveFirstCounterUpdateMessage_PassesItOn() {
		int counter = 3;
		
		Message counterUpdateMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		timeMachineState.update(counterUpdateMessage.toDataTuple());
		
		verify(mockSendToGatekeeper, times(1)).signal(counterUpdateMessage.toDataTuple());
	}
	
	@Test
	public void testReceiveCounterUpdateMessageWhilePlaying_PassesItOn() {
		int counter = 3;
		
		Message counterUpdateMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		timeMachineState.update(counterUpdateMessage.toDataTuple());
		timeMachineState.update(counterUpdateMessage.toDataTuple());
		
		verify(mockSendToGatekeeper, times(2)).signal(counterUpdateMessage.toDataTuple());
	}
	
	@Test
	public void testReceiveCounterUpdateMessageWhilePaused_DoesNothing() {
		int counter = 3;
		
		Message pauseMessage = new Message(MessageType.Pause, nodeId, phone, timestamp);
		Message counterUpdateMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		timeMachineState.update(pauseMessage.toDataTuple());
		timeMachineState.update(counterUpdateMessage.toDataTuple());
		
		verifyZeroInteractions(mockSendToGatekeeper);
	}		
	
	@Test
	public void testReceivePlayMessage_PlaysPhoneBuffer() {
		int counter = 3;
		
		Message pauseMessage = new Message(MessageType.Pause, nodeId, phone, timestamp);
		Message counterUpdateMessage1 = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		Message counterUpdateMessage2 = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp + 1, counter);
		timeMachineState.update(pauseMessage.toDataTuple());
		timeMachineState.update(counterUpdateMessage1.toDataTuple());
		timeMachineState.update(counterUpdateMessage2.toDataTuple());
		
		Message playMessage = new Message(MessageType.Play, nodeId, phone, timestamp);
		timeMachineState.update(playMessage.toDataTuple());
		
		InOrder inOrder = Mockito.inOrder(mockSendToGatekeeper);
		inOrder.verify(mockSendToGatekeeper, times(1)).signal(counterUpdateMessage1.toDataTuple());
		inOrder.verify(mockSendToGatekeeper, times(1)).signal(counterUpdateMessage2.toDataTuple());
	}
	
	@Test
	public void testPauseMessage_SendsPauseConfirmation(){
		int counter = 1;
		Message counterUpdateMessage = 
				new Message(eu.ferari.examples.distributedcount.misc.Message.MessageType.CounterUpdate, nodeId, phone, timestamp, counter);
		Message pauseMessage = new Message(eu.ferari.examples.distributedcount.misc.Message.MessageType.Pause, nodeId, phone, timestamp);
		Message pauseConfirmationMessage = new Message(eu.ferari.examples.distributedcount.misc.Message.MessageType.PauseConfirmation, nodeId, phone, timestamp, counter);
		
		timeMachineState.update(counterUpdateMessage.toDataTuple());
		timeMachineState.update(pauseMessage.toDataTuple());
		
		assertThat(timeMachineState.getState(phone), is(equalTo(PhoneState.Pause)));
		verify(mockSendToCommunicator, times(1)).signal(pauseConfirmationMessage.toDataTuple());
	}	
}
//TODO: use Mockito correctly. See http://www.baeldung.com/mockito-verify