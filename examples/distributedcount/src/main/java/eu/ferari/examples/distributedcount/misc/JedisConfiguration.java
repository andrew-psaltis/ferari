package eu.ferari.examples.distributedcount.misc;

public class JedisConfiguration {
	private String host = "localhost";
	private int port = 6379;
	private int timeout = 0;		
	
	public JedisConfiguration(){}
	
	public JedisConfiguration(String host, int port, int timeout) {
		this.host = host;
		this.port = port;
		this.timeout = timeout;
	}

	public String getHost() {
		return host;
	}
	
	public int getPort() {
		return port;
	}
	
	public int getTimeout() {
		return timeout;
	}		
}
