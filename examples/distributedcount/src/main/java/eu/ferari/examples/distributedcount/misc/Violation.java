package eu.ferari.examples.distributedcount.misc;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.ferari.examples.distributedcount.exceptions.NodeDataAlreadyExistsException;
import eu.ferari.examples.distributedcount.exceptions.NodeDataNotYetCompleteException;
import eu.ferari.examples.distributedcount.exceptions.NodeListCompleteException;

public class Violation {
	private static final Logger logger = LoggerFactory.getLogger(Violation.class);
	public static final int DEFAULT_GLOBAL_THRESHOLD = 9;
	private int globalThreshold = DEFAULT_GLOBAL_THRESHOLD;
	
	private String phone;	
	private int numNodes;
	private long timestamp;
	// Map of nodeId -> [counter, currentThreshold]
	private Map<String, Integer> nodeData = new HashMap<String, Integer>();
		
	public Violation(String phone, int numNodes, long timestamp) {		
		this.phone = phone;
		this.numNodes = numNodes;
		this.timestamp = timestamp;
		logger.debug("Violation opened {}", this);
	}
	
	// TODO: convert all tests to this constructor and delete other constructor
	public Violation(Message thresholdPassMessage, int numNodes){		
		this.phone = thresholdPassMessage.getPhone();
		this.numNodes = numNodes;
		this.timestamp = thresholdPassMessage.getTimestamp();		
	}
	
	public Violation(Message thresholdPassMessage, int numNodes, int globalThreshold){
		this(thresholdPassMessage, numNodes);
		this.globalThreshold = globalThreshold;
		logger.info("Violation opened. {}", this);
	}
	
	// TODO: convert all tests to this method and delete other add signature
	public boolean add(Message thresholdPassMessage) throws NodeDataAlreadyExistsException, NodeListCompleteException {
		return add(
				thresholdPassMessage.getNodeId(), 
				thresholdPassMessage.getTimestamp(),
				thresholdPassMessage.getCounter());
	}
	/**
	 * Add counter data for a node. 
	 * @param nodeId
	 * @param timestamp
	 * @param counter
	 * @param currentThreshold
	 * @return True if all nodes have returned data, false otherwise.
	 * @throws NodeDataAlreadyExistsException 
	 * @throws NodeListCompleteException 
	 */
	public boolean add(String nodeId, long timestamp, 
			int counter) throws NodeDataAlreadyExistsException, NodeListCompleteException {		
		if (nodeData.size() == numNodes)
			throw new NodeListCompleteException();
		
		if (timestamp > this.timestamp){
			logger.error("Timestamp {} is greater than violation's timestamp {}, ignoring", timestamp, this.timestamp);
			throw new IllegalArgumentException("Tried to insert node data with later timestamp " +
			 timestamp + " into node with timestamp " + this.timestamp);
		}
			
		if (timestamp < this.timestamp){
			logger.info("Timestamp {} is smaller than violation's timestamp {}, will reset violation's timestamp and empty all data", 
					timestamp, this.timestamp);
			nodeData.clear();
			this.timestamp = timestamp;						
		}							
		
		if (nodeData.containsKey(nodeId))
			throw new NodeDataAlreadyExistsException(nodeId);
		
		nodeData.put(nodeId, counter);
		
		logger.info("Added node {} to violation. {} and counter {}. {} out of {} nodes", nodeId, this, counter, nodeData.size(), numNodes);
		
		return nodeData.size() == numNodes;
	}
	
	public int getNewThreshold(String nodeId) throws NodeDataNotYetCompleteException {
		if (nodeData.size() != numNodes)
			throw new NodeDataNotYetCompleteException();
		logger.info("Getting new threshold for node {}", nodeId);
		int counter = nodeData.get(nodeId);
		int globalCount = getGlobalCount();
		
		int newThreshold = (globalThreshold - globalCount) / numNodes + counter;
		logger.debug("New threshold calculation for node {}: ({} - {}) / {} + {}", nodeId, globalThreshold, globalCount, numNodes, counter);
		logger.info("New threshold {} calculated for phone {} at node {} at timestamp {}", 
				newThreshold, phone, nodeId, timestamp);
		return newThreshold;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Phone: ");
		sb.append(phone);
		sb.append(", Timestamp: ");
		sb.append(timestamp);
		sb.append(", Global threshold: ");
		sb.append(globalThreshold);
		sb.append(" , Count: ");
		sb.append(getGlobalCount());
		return sb.toString();
	}
	
	/**
	 * Only for unit testing.
	 * @return
	 */
	public int getNumReportedNodes(){
		return nodeData.size();
	}
	
	public String getPhone(){
		return phone;
	}
	
	public long getTimestamp(){
		return timestamp;
	}
	
	public int getGlobalThreshold(){
		return globalThreshold;
	}
	
	public int getGlobalCount(){
		int globalCount = 0;
		for (int counter : nodeData.values()) {
			globalCount += counter;
		}
		
		return globalCount;
	}
}
