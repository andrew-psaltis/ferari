package eu.ferari.examples.distributedcount.misc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import eu.ferari.core.interfaces.IState;
import redis.clients.jedis.JedisPubSub;

public class RedisSubscriber extends JedisPubSub {
	private static final Logger logger = LoggerFactory.getLogger(RedisSubscriber.class);
	
	private final IState state;
	private final Gson gson = new Gson();
	
	public RedisSubscriber(IState state) {
		this.state = state;
		logger.info("Redis subscriber ready");
	}

	@Override
	public void onMessage(String channel, String json) {
		logger.info("Received message {}", json);
		Message message = gson.fromJson(json, Message.class);
		state.update(message.toDataTuple());	
	}

	@Override
	public void onPMessage(String pattern, String channel, String message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSubscribe(String channel, int subscribedChannels) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUnsubscribe(String channel, int subscribedChannels) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPUnsubscribe(String pattern, int subscribedChannels) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPSubscribe(String pattern, int subscribedChannels) {
		// TODO Auto-generated method stub

	}

}
