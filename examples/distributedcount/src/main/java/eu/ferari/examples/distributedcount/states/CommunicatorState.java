package eu.ferari.examples.distributedcount.states;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.IState;
import eu.ferari.examples.distributedcount.misc.DefaultHashMap;
import eu.ferari.examples.distributedcount.misc.Message;
import eu.ferari.examples.distributedcount.misc.Message.MessageType;

public class CommunicatorState implements IState {
	private static final Logger logger = LoggerFactory.getLogger(CommunicatorState.class);
	
	public enum State {
		Monitoring,
		PausingCounterRequestMessage,
		PausingLocalThresholdCrossing,
		WaitingForCoordinator
	}
	
	private ISend sendToTimeMachine;	
	private ISend sendToGatekeeper;
	private ISend sendToCoordinator;
	private String nodeId = null;
	private Map<String, State> phoneStates = new DefaultHashMap<String, State>(State.Monitoring); 
	private Map<String, Long> timeRegister = new DefaultHashMap<String, Long>(-1l); 
	
	public CommunicatorState(ISend sendToTimeMachine, ISend sendToGatekeeper,
			ISend sendToCoordinator) {
		this.sendToTimeMachine = sendToTimeMachine;
		this.sendToGatekeeper = sendToGatekeeper;
		this.sendToCoordinator = sendToCoordinator;
	}
	
	public CommunicatorState(ISend sendToTimeMachine, ISend sendToGatekeeper,
			ISend sendToCoordinator, String nodeId) {
		this(sendToTimeMachine, sendToGatekeeper, sendToCoordinator);
		this.nodeId = nodeId;
	}

	@Override
	public void update(DataTuple data) {
		Message message = new Message(data);
		
		if (nodeId == null && message.getType().equals(MessageType.CounterUpdate)){
			logger.info("Setting nodeId to {}", message.getNodeId());
			logger.debug("nodeId set from message {}", message);
			nodeId = message.getNodeId();			
		}
			
		
		switch (message.getType()) {
		case CounterUpdate:
			onCounterUpdate(message);
			break;
		case PauseConfirmation:
			onPauseConfirmation(message);
			break;	
		case CounterRequest:
			onCounterRequest(message);
			break;
		case UpdateThreshold:
			onUpdateThreshold(message);
			break;
		default:
			throw new IllegalArgumentException("Don't know how to handle message of type " + message.getType());
		}
	}	

	public State getState(String phone){
		return phoneStates.get(phone);
	}
	
	/**
	 * For unit testing purposes only. 
	 * @param nodeId
	 */
	public void setNodeId(String nodeId){
		this.nodeId = nodeId;
	}
	
	public long getRegisteredTime(String phone){
		return timeRegister.get(phone);
	}
	
	public void setStateAndTime(String phone, State state, long timestamp){
		logger.info("Switching phone {} at node {} to state {} at timestamp {}", phone, nodeId, state, timestamp);
		phoneStates.put(phone, state);
		timeRegister.put(phone, timestamp);
	}
	
	private void onCounterUpdate(Message message){
		logMessageReceived(message);
		
		String phone = message.getPhone();
		long timestamp = message.getTimestamp();
		
		switch (getState(phone)) {
		case Monitoring:
			setStateAndTime(phone, State.PausingLocalThresholdCrossing, timestamp);
			sendToTimeMachine.signal(message.toPauseMessage(nodeId).toDataTuple());
			break;
		case PausingCounterRequestMessage:
			if (timestamp < timeRegister.get(phone))
				setStateAndTime(phone, State.PausingLocalThresholdCrossing, timestamp);
			break;
		case PausingLocalThresholdCrossing:			
			break;
		case WaitingForCoordinator:
			logger.error("Received counter update while waiting for coordinator");
			break;
		default:
			throw new IllegalArgumentException("Don't know how to handle state " + getState(phone));
		}
	}
	
	private void onPauseConfirmation(Message message){
		logMessageReceived(message);
		
		String phone = message.getPhone();
		switch (getState(phone)) {
		case Monitoring:
			logger.error("Received pause confirmation while monitoring");
			break;
		case PausingCounterRequestMessage:
		case PausingLocalThresholdCrossing:
			Message counterReplyMessage = 
				new Message(MessageType.CounterUpdate, message.getNodeId(), phone, message.getTimestamp(), message.getCounter());
			setStateAndTime(phone, State.WaitingForCoordinator, message.getTimestamp());
			sendToCoordinator.signal(counterReplyMessage.toDataTuple());
			break;					
		case WaitingForCoordinator:
			logger.error("Received pause confirmation while waiting for coordinator");
			break;
		default:
			throw new IllegalArgumentException("Don't know how to handle state " + getState(phone));
		}
	}
	
	private void onCounterRequest(Message message){
		logMessageReceived(message);
		
		if (message.getNodeId().equals(nodeId)){
			logger.debug("Ignoring counter request for node {} because this nodeId is {}", message.getNodeId(), nodeId);
			return;
		}
		
		String phone = message.getPhone();
		
		switch (getState(phone)) {
		case Monitoring:
			setStateAndTime(phone, State.PausingCounterRequestMessage, message.getTimestamp());
			sendToTimeMachine.signal(message.toPauseMessage(nodeId).toDataTuple());
			break;
		case PausingCounterRequestMessage:
		case PausingLocalThresholdCrossing:
			if (message.getTimestamp() < timeRegister.get(phone)){
				logger.debug("Received counter request for phone {} at time {} while it was at time {}. Will switch to earlier time.", 
						phone, message.getTimestamp(), timeRegister.get(message.getTimestamp()));
				setStateAndTime(phone, State.PausingCounterRequestMessage, message.getTimestamp());
			}				
			break;
		case WaitingForCoordinator:
			if (message.getTimestamp() < getRegisteredTime(phone)){
				logger.debug("Received counter request for phone {} at time {} while it was at time {}. Will switch to earlier time.", 
						phone, message.getTimestamp(), getRegisteredTime(phone));
				setStateAndTime(phone, State.PausingCounterRequestMessage, message.getTimestamp());
				sendToTimeMachine.signal(message.toPauseMessage(nodeId).toDataTuple());
			}			
			break;
		default:
			throw new IllegalArgumentException("Don't know how to handle state " + getState(phone));
		}
	}
	
	private void onUpdateThreshold(Message message) {
		logMessageReceived(message);
		
		if (!message.getNodeId().equals(nodeId)){
			logger.debug("Ignoring threshold update message for node {} because this node is {}", message.getNodeId(), nodeId);
			return;
		}
		
		switch (getState(message.getPhone())) {
		case Monitoring:
		case PausingCounterRequestMessage:
		case PausingLocalThresholdCrossing:
			logger.error("Received threshold update while in {} state", getState(message.getPhone()));
			break;
		case WaitingForCoordinator:
			Message newThresholdMessage = new Message(MessageType.NewThreshold, message.getNodeId(), message.getPhone(), message.getNewThreshold());
			Message playMessage = new Message(MessageType.Play, message.getNodeId(), message.getPhone(), message.getTimestamp());
			unsetStateAndTime(message.getPhone());
			sendToGatekeeper.signal(newThresholdMessage.toDataTuple());
			sendToTimeMachine.signal(playMessage.toDataTuple());
			break;
		default:
			throw new IllegalArgumentException("Don't know how to handle state " + getState(message.getPhone()));
		}		
	}
	
	private void unsetStateAndTime(String phone){
		logger.info("Deleting state and timestamp for phone {} at node {}", phone, nodeId);
		phoneStates.remove(phone);
		timeRegister.remove(phone);
	}
	
	private void logMessageReceived(Message message){
		logger.info("Received message. {}, when state is {}", message, getState(message.getPhone()));
		for (Map.Entry<String, State> kvp : phoneStates.entrySet()) {
			logger.debug("State of node {}: {} -> {} @ timestamp {}", nodeId, kvp.getKey(), kvp.getValue(), timeRegister.get(kvp.getKey()));
		}
	}
}
