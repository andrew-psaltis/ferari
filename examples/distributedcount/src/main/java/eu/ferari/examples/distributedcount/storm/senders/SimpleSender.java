package eu.ferari.examples.distributedcount.storm.senders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.task.OutputCollector;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

public class SimpleSender implements ISend {
	private static final Logger logger = LoggerFactory.getLogger(SimpleSender.class);
	
	private OutputCollector collector;
	
	public SimpleSender(OutputCollector collector) {
		this.collector = collector;
	}

	@Override
	public void signal(DataTuple data) {
		logger.info("Simple sender emitting message");
		collector.emit(data.asList());
	}

}
