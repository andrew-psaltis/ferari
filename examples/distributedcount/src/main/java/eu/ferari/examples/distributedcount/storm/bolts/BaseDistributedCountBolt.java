package eu.ferari.examples.distributedcount.storm.bolts;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.IState;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

public abstract class BaseDistributedCountBolt extends BaseRichBolt {
	private static final Logger logger = LoggerFactory.getLogger(BaseDistributedCountBolt.class);
	
	protected IState state;

	protected OutputCollector collector;
	protected TopologyContext context;
	
	public static final Fields DEFAULT_OUTPUT_FIELDS = 
			new Fields("type", "nodeId", "phone", "timestamp", "counter");	

	@Override
	public void execute(Tuple tuple) {			
		String nodeId = "none";
		try {
			nodeId = tuple.getStringByField("nodeId");
		} catch (IllegalArgumentException e) {
			logger.warn("Source: {}", tuple.getSourceComponent());
		} 
		
		logger.info("{} => {} ({}, {}), task index: {}, node: {}", 						
						tuple.getSourceComponent(),
						context.getComponentId(context.getThisTaskId()),
						tuple.getString(0),
						tuple.getSourceStreamId(), 						
						context.getThisTaskIndex(),
						nodeId
				);		
		state.update(new DataTuple(tuple.getValues()));
		collector.ack(tuple);
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		return null;
	}

	@Override
	public void prepare(Map conf, TopologyContext context, OutputCollector collector){
		this.collector = collector;
		this.context = context;
	}

	@Override
	public abstract void declareOutputFields(OutputFieldsDeclarer declarer);
}
