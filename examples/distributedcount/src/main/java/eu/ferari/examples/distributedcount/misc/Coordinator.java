package eu.ferari.examples.distributedcount.misc;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.distributedcount.Main.Sender;
import eu.ferari.examples.distributedcount.bare.senders.RedisSender;
import eu.ferari.examples.distributedcount.states.CoordinatorState;
import eu.ferari.examples.distributedcount.storm.DistributedCountTopology;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

public class Coordinator {
	private static final Logger logger = LoggerFactory.getLogger(Coordinator.class);
	
	private final JedisPool jedisPool;
	private final Jedis publisherJedis;
	private final Jedis subscriberJedis;
	private final String incomingChannel;
	private final JedisPubSub subscriber;
	private final CoordinatorState state;
	
	public Coordinator(String propertiesFile) throws IOException {
		Properties properties = Utils.loadProperties(propertiesFile, this);
		String commToCoordinatorChannel = properties.getProperty(DistributedCountTopology.ConfigKeys.commToCoordinatorChannel.toString());
		String coordinatorToCommChannel = properties.getProperty(DistributedCountTopology.ConfigKeys.coordinatorToCommChannel.toString());
		int numNodes = Integer.valueOf(properties.getProperty(DistributedCountTopology.ConfigKeys.numBillingNodes.toString()));
		int globalThreshold = Integer.valueOf(properties.getProperty(DistributedCountTopology.ConfigKeys.globalThreshold.toString()));
		final JedisPoolConfig poolConfig = new JedisPoolConfig();
		JedisConfiguration jedisConfig = new JedisConfiguration();
		this.jedisPool = new JedisPool(poolConfig, jedisConfig.getHost(), jedisConfig.getPort(), jedisConfig.getTimeout());
		publisherJedis = jedisPool.getResource();
		subscriberJedis = jedisPool.getResource();
		incomingChannel = commToCoordinatorChannel;
		ISend sender = new RedisSender(publisherJedis, coordinatorToCommChannel);
		state = new CoordinatorState(sender, sender, numNodes, globalThreshold);
		subscriber = new RedisSubscriber(state);
		logger.info("Coordinator ready");
	}
	
	/**
	 * Constructor used for testing.
	 * @param propertiesFile
	 * @param globalViolationReporter
	 * @throws IOException
	 */
	public Coordinator(String propertiesFile, ISend globalViolationReporter) throws IOException {
		this(propertiesFile);
		state.setViolationReporter(globalViolationReporter);
	}
	
	
	public void start() {
		new Thread(new Runnable() {			
			@Override
			public void run() {
				logger.info("Coordinator's subscriber started");
				subscriberJedis.subscribe(subscriber, incomingChannel);
				logger.info("Subscriber done");
			}
		}).start();				
	}
	
	public void stop(){
		logger.info("Shutting down coordinator");
		subscriber.unsubscribe();
		jedisPool.returnResource(subscriberJedis);
		jedisPool.returnResource(publisherJedis);
		jedisPool.close();
	}
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1)
			throw new Exception("Usage: Coordinator propertiesFile");
		
		final JedisPoolConfig poolConfig = new JedisPoolConfig();
		JedisConfiguration jedisConfig = new JedisConfiguration();		
		try (JedisPool jedisPool = new JedisPool(poolConfig, jedisConfig.getHost(), jedisConfig.getPort(), jedisConfig.getTimeout())){
			Jedis publisherJedis = jedisPool.getResource();		
			ISend violationReporter = new Sender(publisherJedis, "violations");
			Coordinator coordinator = new Coordinator(args[0], violationReporter);
			coordinator.start();
		}		
		// TODO: trap ctrl+c and exit gracefullly
	}
}
