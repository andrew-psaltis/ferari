package eu.ferari.examples.distributedcount.exceptions;

public class NodeDataNotYetCompleteException extends Exception {

	public NodeDataNotYetCompleteException() {
		super("Node data not yet complete");
	}

}
