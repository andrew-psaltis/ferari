package eu.ferari.examples.distributedcount.storm.bolts;

import java.util.Map;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.IState;
import eu.ferari.examples.distributedcount.states.TimeMachineState;
import eu.ferari.examples.distributedcount.storm.senders.DirectSender;

public class TimeMachineBolt extends BaseDistributedCountBolt {
	private static final long serialVersionUID = 8290306847153126869L;
	
	private String gatekeeperName;
	private String communicatorName;
	private	String toGatekeeperStreamId;
	private String toCommunicatorStreamId;
	
	public TimeMachineBolt(String gatekeeperName, String communicatorName, String toGatekeeperStreamId, String toCommunicatorStreamId){
		this.gatekeeperName = gatekeeperName;
		this.communicatorName = communicatorName;
		this.toGatekeeperStreamId = toGatekeeperStreamId;
		this.toCommunicatorStreamId = toCommunicatorStreamId;
	}	

	@Override
	public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
		super.prepare(conf, context, collector);
		ISend sendToGatekeeper = new DirectSender(collector, context, gatekeeperName, toGatekeeperStreamId);
		ISend sendToCommunicator = new DirectSender(collector, context, communicatorName, toCommunicatorStreamId);
		this.state = new TimeMachineState(sendToGatekeeper, sendToCommunicator);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(toGatekeeperStreamId, true, DEFAULT_OUTPUT_FIELDS);
		declarer.declareStream(toCommunicatorStreamId, true, DEFAULT_OUTPUT_FIELDS);
	}
}
