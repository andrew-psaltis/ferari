package eu.ferari.examples.distributedcount.storm.bolts;

import java.util.Map;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.IState;
import eu.ferari.examples.distributedcount.bare.senders.RedisSender;
import eu.ferari.examples.distributedcount.misc.JedisConfiguration;
import eu.ferari.examples.distributedcount.states.CommunicatorState;
import eu.ferari.examples.distributedcount.storm.senders.DirectSender;
import eu.ferari.examples.distributedcount.storm.senders.SimpleSender;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;

public class CommunicatorBolt extends BaseDistributedCountBolt {
	private static final Fields NEW_THRESHOLD_FIELDS = new Fields("type", "nodeId", "phone", "newThreshold");
	private static final Fields PAUSE_FIELDS = new Fields("type", "nodeId", "phone", "timestamp");
	
	private final String timeMachineName;
	private final String gatekeeperName;
	private final String toTimeMachineStreamId;
	private final String toGatekeeperStreamId;
	private final String commToCoordinatorChannel;
	
	private JedisPool jedisPool;
	private Jedis publisher;
	private ISend sendToCoordinator;

	public CommunicatorBolt(String timeMachineName, String gatekeeperName,
			String toTimeMachineStreamId,
			String toGatekeeperStreamId,
			String commToCoordinatorChannel) {
		this.timeMachineName = timeMachineName;
		this.gatekeeperName = gatekeeperName;
		this.toTimeMachineStreamId = toTimeMachineStreamId;
		this.toGatekeeperStreamId = toGatekeeperStreamId;	
		this.commToCoordinatorChannel = commToCoordinatorChannel;
	}	
	
	@Override
	public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
		super.prepare(conf, context, collector);
		ISend sendToTimeMachine = new DirectSender(collector, context, timeMachineName, toTimeMachineStreamId);
		ISend sendToGatekeeper = new DirectSender(collector, context, gatekeeperName, toGatekeeperStreamId);
		final JedisPoolConfig poolConfig = new JedisPoolConfig();
		JedisConfiguration jedisConfig = new JedisConfiguration();
		this.jedisPool = new JedisPool(poolConfig, jedisConfig.getHost(), jedisConfig.getPort(), jedisConfig.getTimeout());
		publisher = jedisPool.getResource();	
		sendToCoordinator = new RedisSender(publisher, commToCoordinatorChannel);
		
		String nodeId = (String) conf.get("nodeId");
		this.state = new CommunicatorState(sendToTimeMachine, sendToGatekeeper, sendToCoordinator, nodeId);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(toTimeMachineStreamId, true, PAUSE_FIELDS);
		declarer.declareStream(toGatekeeperStreamId, true, NEW_THRESHOLD_FIELDS);
		declarer.declare(new Fields("message"));
	}
	
	@Override
	public void cleanup() {
		super.cleanup();
		jedisPool.returnResource(publisher);
		jedisPool.close();
	}
}
