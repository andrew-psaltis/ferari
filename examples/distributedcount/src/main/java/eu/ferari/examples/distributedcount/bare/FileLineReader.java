package eu.ferari.examples.distributedcount.bare;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import backtype.storm.tuple.Values;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

public class FileLineReader {
	private final String filepath;
	private final int lineDivider;
	private final ISend sendToTimeMachine;	
	
	public FileLineReader(String filepath, int lineDivider, ISend sendToTimeMachine) {
		this.filepath = filepath;
		this.lineDivider = lineDivider;
		this.sendToTimeMachine = sendToTimeMachine;
	}
	
	public void readAllLines() throws IOException, FileNotFoundException {
		BufferedReader reader = new BufferedReader(new FileReader(filepath));
		int lineCounter = 0;
		for (String line, j; (line = reader.readLine()) != null; ) {
			lineCounter++;
			if (lineCounter % lineDivider != 0)
				continue;
			
			String[] parts = line.split(",");
		    Values values = new Values();
		    for (int i = 0; i < parts.length; i++) {
		    	switch (i) {
				case 0:
				case 1:
				case 2:
					values.add(parts[i]);
					break;
				case 3:
					values.add(Integer.parseInt(parts[i]));
					break;
				case 4:
					values.add(Long.parseLong(parts[i]));
					break;										
				default:
					throw new IllegalArgumentException("Don't know how to handle index " + i);
				}
		    }
		    reader.close();
		    sendToTimeMachine.signal(new DataTuple(Arrays.asList(values.toArray())));
		}
	}
}
