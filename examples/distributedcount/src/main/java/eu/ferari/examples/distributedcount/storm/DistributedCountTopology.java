package eu.ferari.examples.distributedcount.storm;

import java.io.IOException;
import java.util.Properties;

import eu.ferari.examples.distributedcount.misc.Utils;
import eu.ferari.examples.distributedcount.storm.bolts.CommunicatorBolt;
import eu.ferari.examples.distributedcount.storm.bolts.EsperBolt;
import eu.ferari.examples.distributedcount.storm.bolts.GatekeeperBolt;
import eu.ferari.examples.distributedcount.storm.bolts.TimeMachineBolt;
import eu.ferari.examples.distributedcount.storm.spouts.FileSpout;
import eu.ferari.examples.distributedcount.storm.spouts.RedisPubSubSpout;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.topology.BoltDeclarer;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseRichSpout;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DistributedCountTopology {
	private static final Logger logger = LoggerFactory.getLogger(DistributedCountTopology.class);
	
	public static enum ConfigKeys {
		nodeId, globalThreshold, commToCoordinatorChannel, coordinatorToCommChannel, numBillingNodes
	}
	
	private static final String FILE_READER_SPOUT_NAME = "filereader_spout";
	private static final String COMMUNICATOR_SPOUT_NAME = "communicator_spout";
	
	private static final String ESPER_BOLT_NAME = "esper_bolt";
	private static final String TIME_MACHINE_BOLT_NAME = "timemachine";
	private static final String GATEKEEPER_BOLT_NAME = "gatekeeper";
	private static final String COMMUNICATOR_BOLT_NAME = "communicator";
	
	private static final String DEFAULT_STREAM_ID = "default_stream";
	private static final String TIMEMACHINE_TO_COMMUNICATOR_STREAM_ID = "timemachine_to_communicator";
	private static final String COMMUNICATOR_TO_GATEKEEPER_STREAM_ID = "communicator_to_gatekeeper";
	private static final String COMMUNICATOR_TO_TIMEMACHINE_STREAM_ID = "communicator_to_timemachine";
	private static final String COORDINATOR_TO_COMM_THRESH_UPDATE_STREAM_ID = "coordinator_to_comm_thresh_pass";
	private static final String COORDINATOR_TO_COMM_COUNTER_REQUEST_STREAM_ID = "coordinator_to_comm_counter_request";	
	
	private final Properties properties;
	private final int numBillingNodes;	
	private final int globalThreshold;
	private final String commToCoordinatorChannel;
	private final String coordinatorToCommChannel;
	private final String inputFilepath;
	
	private final LocalCluster cluster = new LocalCluster();
	
	public DistributedCountTopology(String propertiesFile, String inputFile) throws IOException{
		properties = Utils.loadProperties(propertiesFile, this);		
		this.commToCoordinatorChannel = properties.getProperty(ConfigKeys.commToCoordinatorChannel.toString());
		this.coordinatorToCommChannel = properties.getProperty(ConfigKeys.coordinatorToCommChannel.toString());
		this.numBillingNodes = Integer.valueOf(properties.getProperty(ConfigKeys.numBillingNodes.toString()));
		this.globalThreshold = Integer.valueOf(properties.getProperty(ConfigKeys.globalThreshold.toString()));
		this.inputFilepath = this.getClass().getClassLoader().getResource(inputFile).getPath();
	}
	
	public static void main(String[] args) throws Exception {
		if (args.length < 2)
			throw new Exception("Usage: java DistributedCountTopology propertiesFile inputFile (timeout)");
		String propertiesFile = args[0];
		String inputFile = args[1];
		DistributedCountTopology topology = new DistributedCountTopology(propertiesFile, inputFile);
		topology.start();
		if (args[2] != null) {
			int timeout = Integer.parseInt(args[2]);
			backtype.storm.utils.Utils.sleep(timeout);
			topology.stop();
		}
	}
	
	public void start() throws IOException {
		logger.info("Setting up topology");		
		Config stormConfig = Utils.propertiesToStormConfiguration(properties);
		stormConfig.setMaxTaskParallelism(1);
		
        TopologyBuilder builder = new TopologyBuilder();
        
        BaseRichSpout fileSpout = new FileSpout(inputFilepath, 5, 10, 2, "yyyy-MM-dd HH:mm:ss");  
        BaseRichSpout communicatorRedisSpout = new RedisPubSubSpout(coordinatorToCommChannel, COORDINATOR_TO_COMM_COUNTER_REQUEST_STREAM_ID, COORDINATOR_TO_COMM_THRESH_UPDATE_STREAM_ID);
        
        IRichBolt esperBolt = new EsperBolt();        
        IRichBolt timeMachineBolt = new TimeMachineBolt(
        		GATEKEEPER_BOLT_NAME, 
        		COMMUNICATOR_BOLT_NAME, 
        		DEFAULT_STREAM_ID, 
        		TIMEMACHINE_TO_COMMUNICATOR_STREAM_ID
        	);
        IRichBolt gatekeeperBolt = new GatekeeperBolt(
        		COMMUNICATOR_BOLT_NAME, 
        		DEFAULT_STREAM_ID,
        		globalThreshold / numBillingNodes
        	);
        IRichBolt communicatorBolt = new CommunicatorBolt(
        		TIME_MACHINE_BOLT_NAME, 
        		GATEKEEPER_BOLT_NAME, 
        		COMMUNICATOR_TO_TIMEMACHINE_STREAM_ID, 
        		COMMUNICATOR_TO_GATEKEEPER_STREAM_ID,
        		commToCoordinatorChannel
        	);
        
        builder.setSpout(FILE_READER_SPOUT_NAME, fileSpout, 1);
        builder.setSpout(COMMUNICATOR_SPOUT_NAME, communicatorRedisSpout, 1);
        BoltDeclarer esperBoltDeclarer = builder.setBolt(ESPER_BOLT_NAME, esperBolt, 1);
        BoltDeclarer timeMachineBoltDeclarer = 
        		builder.setBolt(TIME_MACHINE_BOLT_NAME, timeMachineBolt, 1);        
        BoltDeclarer gatekeeperBoltDeclarer = 
        		builder.setBolt(GATEKEEPER_BOLT_NAME, gatekeeperBolt, 1);        
        BoltDeclarer communicatorBoltDeclarer = 
        		builder.setBolt(COMMUNICATOR_BOLT_NAME, communicatorBolt, 1);                
                
        esperBoltDeclarer.shuffleGrouping(FILE_READER_SPOUT_NAME);
//        communicatorBoltDeclarer.allGrouping(COMMUNICATOR_SPOUT_NAME, COORDINATOR_TO_COMM_COUNTER_REQUEST_STREAM_ID);
       // communicatorBoltDeclarer.allGrouping(COMMUNICATOR_SPOUT_NAME, COORDINATOR_TO_COMM_THRESH_UPDATE_STREAM_ID);
        timeMachineBoltDeclarer.shuffleGrouping(ESPER_BOLT_NAME);
        gatekeeperBoltDeclarer.directGrouping(TIME_MACHINE_BOLT_NAME, DEFAULT_STREAM_ID);
        communicatorBoltDeclarer.directGrouping(GATEKEEPER_BOLT_NAME, DEFAULT_STREAM_ID);
        communicatorBoltDeclarer.directGrouping(TIME_MACHINE_BOLT_NAME, TIMEMACHINE_TO_COMMUNICATOR_STREAM_ID);
        gatekeeperBoltDeclarer.directGrouping(COMMUNICATOR_BOLT_NAME, COMMUNICATOR_TO_GATEKEEPER_STREAM_ID);
        timeMachineBoltDeclarer.directGrouping(COMMUNICATOR_BOLT_NAME, COMMUNICATOR_TO_TIMEMACHINE_STREAM_ID);
                
        logger.info("Submitting topology");
        String topologyName = "distributed-count-" + properties.getProperty(ConfigKeys.nodeId.toString());
        cluster.submitTopology(topologyName, stormConfig, builder.createTopology());
	}
	
	public void stop(){
		logger.info("Shutting down topology");
		cluster.shutdown();
	}
	
}
