package eu.ferari.examples.distributedcount.misc;

import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;
import redis.clients.jedis.exceptions.JedisConnectionException;

public class RedisListenerThread extends Thread {
	private static final Logger logger = LoggerFactory.getLogger(RedisListenerThread.class);
	
	private final LinkedBlockingQueue<String> queue;
	private final JedisPool pool;
	private final String channel;
	private JedisPubSub subscriber;
	
	public RedisListenerThread(LinkedBlockingQueue<String> queue, JedisPool pool, String channel) {
		this.queue = queue;
		this.pool = pool;
		this.channel = channel;
	}

	public void run() {
		logger.info("Starting to listen");
		subscriber = new JedisPubSub() {

			@Override
			public void onMessage(String channel, String message) {
				logger.info("Received message {}", message);
				boolean isAdded = queue.offer(message);
				if (!isAdded)
					logger.error("Redis queue is full");
			}

			@Override
			public void onPMessage(String pattern, String channel, String message) {}
			@Override
			public void onPSubscribe(String channel, int subscribedChannels) {}
			@Override
			public void onPUnsubscribe(String channel, int subscribedChannels) {}
			@Override
			public void onSubscribe(String channel, int subscribedChannels) {}
			@Override
			public void onUnsubscribe(String channel, int subscribedChannels) {}
		};
		
		Jedis jedisSubscriber = pool.getResource();		
		try {
			jedisSubscriber.subscribe(subscriber, channel);
		} finally {			
			pool.returnResource(jedisSubscriber);
			logger.info("Done listening to Redis");
		}
	}
	
	public void quit(){
		logger.info("Going to quit Redis");
		subscriber.unsubscribe();
	}
}
