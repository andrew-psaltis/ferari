package eu.ferari.examples.distributedcount.bare;

public class Cdr {
	private final String nodeId;
	private final String phone;
	private final long timestamp;
	
	public Cdr(String nodeId, String phone, long timestamp) {
		this.nodeId = nodeId;
		this.phone = phone;
		this.timestamp = timestamp;
	}

	public String getNodeId() {
		return nodeId;
	}

	public String getPhone() {
		return phone;
	}

	public long getTimestamp() {
		return timestamp;
	}		
}
