package eu.ferari.examples.distributedcount.bare;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.IState;
import eu.ferari.examples.distributedcount.bare.senders.QueueSender;
import eu.ferari.examples.distributedcount.bare.senders.SimpleSender;
import eu.ferari.examples.distributedcount.misc.Violation;
import eu.ferari.examples.distributedcount.states.CommunicatorState;
import eu.ferari.examples.distributedcount.states.GatekeeperState;
import eu.ferari.examples.distributedcount.states.TimeMachineState;

public class BareTopology implements Runnable {
	private static final int NUM_NODES = 3;
	private static final int DEFAULT_LOCAL_THRESHOLD = 10 / NUM_NODES; // FIXME: change value
	
	private final FileLineReader fileReader;
	
	public BareTopology(int lineDivider) {	
		SimpleSender fileReaderToTimeMachine = new SimpleSender();
		this.fileReader = new FileLineReader("", lineDivider, fileReaderToTimeMachine);
		
		SimpleSender timeMachineToGatekeeper = new SimpleSender();
		SimpleSender timeMachineToCommunicator = new SimpleSender();
		IState timeMachineState = new TimeMachineState(timeMachineToGatekeeper, timeMachineToCommunicator);
		
		SimpleSender gatekeeperToCommunicator = new SimpleSender();
		IState gatekeeperState = new GatekeeperState(gatekeeperToCommunicator, DEFAULT_LOCAL_THRESHOLD);
		
		SimpleSender communicatorToTimeMachine = new SimpleSender();
		SimpleSender communicatorToGatekeeper = new SimpleSender();
		ISend communicatorToQueue = new QueueSender();
		IState communicatorState = new CommunicatorState(communicatorToTimeMachine, communicatorToGatekeeper, communicatorToQueue);								
		
		timeMachineToGatekeeper.setTarget(gatekeeperState);
		timeMachineToCommunicator.setTarget(communicatorState);
		
		gatekeeperToCommunicator.setTarget(communicatorState);
		
		communicatorToTimeMachine.setTarget(timeMachineState);
		communicatorToGatekeeper.setTarget(gatekeeperState);
		
		
	}

	public static void main(String[] args) {
		// TODO: read from file
		// Create threadpool of size NUM_NODES. See http://www.vogella.com/tutorials/JavaConcurrency/article.html#threadpools
		ExecutorService executor = Executors.newFixedThreadPool(NUM_NODES);
		for (int i = 0; i < NUM_NODES; i++) {
			BareTopology billingNode = new BareTopology(i + 1);
			executor.execute(billingNode);
		}
	    
		// Create coordinator
	    try {
			executor.awaitTermination(20, TimeUnit.SECONDS);
			System.out.println("Finished all threads");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
	}

	@Override
	public void run() {
		try {
			fileReader.readAllLines();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
