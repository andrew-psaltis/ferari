package eu.ferari.examples.distributedcount.bare;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CdrFromFileProducer implements Iterator<Cdr>{
	private static final Logger logger = LoggerFactory.getLogger(CdrFromFileProducer.class);
	private final LineIterator lineIterator;
	
	public CdrFromFileProducer(String filepath) throws IOException {
		lineIterator = FileUtils.lineIterator(new File(filepath), "UTF-8");
	}

	@Override
	public boolean hasNext() {
		return lineIterator.hasNext();
	}

	@Override
	public Cdr next() {
		String line = lineIterator.nextLine();
		String[] parts = line.split(",");
		String nodeId = parts[0];
		String phone = parts[1];
		long timestamp = Long.parseLong(parts[2]);
		
		return new Cdr(nodeId, phone, timestamp);
	}

	@Override
	public void remove() {
		lineIterator.close();
	}	
}
