package eu.ferari.examples.distributedcount.storm.bolts;

import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPServiceDestroyedException;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;
import com.espertech.esper.client.deploy.DeploymentException;
import com.espertech.esper.client.deploy.ParseException;
import com.espertech.esper.client.time.CurrentTimeEvent;

import eu.ferari.examples.distributedcount.misc.CdrEvent;
import eu.ferari.examples.distributedcount.misc.Message;
import eu.ferari.examples.distributedcount.misc.Message.MessageType;

public class EsperBolt extends BaseRichBolt implements UpdateListener {
	private static final long serialVersionUID = -5558588251440601447L;

	private static final Logger logger = LoggerFactory.getLogger(EsperBolt.class);
	
	private static final int TIME_WINDOW_LENGTH = 300000; // In milliseconds
	private static final String CONTEXT_EXPRESSION = "create context Every10Sec start @now end after " + 
			TIME_WINDOW_LENGTH / 1000 + " sec";
	private static final String COUNT_EXPRESSION = "context Every10Sec select phone, count(*), sum(timestamp) from CdrEvent group by phone output snapshot when terminated";
	
	private OutputCollector collector;
	private EPServiceProvider epService;	
	
	private int timeWindow;	
	
	private String nodeId;

	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
		this.nodeId = (String) stormConf.get("nodeId");
		
		Configuration configuration = new Configuration();
        configuration.getEngineDefaults().getThreading().setInternalTimerEnabled(false);
        configuration.addEventType("CdrEvent", CdrEvent.class.getName());
        
        epService = EPServiceProviderManager.getProvider(nodeId, configuration);
        // Set time to zero. Important to do this before creating the statements.
        epService.getEPRuntime().sendEvent(new CurrentTimeEvent(0));
                
        try {
			epService.getEPAdministrator().getDeploymentAdmin().parseDeploy(CONTEXT_EXPRESSION);
		} catch (EPServiceDestroyedException | IOException | ParseException
				| DeploymentException e) {
			logger.error("Error deploying context expression", e);
			throw new RuntimeException(e);
		}
        EPStatement statement = epService.getEPAdministrator().createEPL(COUNT_EXPRESSION);
        statement.addListener(this); 		
		logger.info("Finished preparing Esper bolt. Node ID: {}", nodeId);
	}

	@Override
	public void execute(Tuple tuple) {
		CdrEvent cdr = new CdrEvent(tuple);
		logger.debug("Received CDR: {}", cdr);
		int eventTimeWindow = cdr.getTimeWindow(TIME_WINDOW_LENGTH);
		if (eventTimeWindow > timeWindow){
			// Advance time 
            timeWindow = eventTimeWindow;
            logger.info("Advancing time window to {}", timeWindow);
            epService.getEPRuntime().sendEvent(new CurrentTimeEvent(timeWindow * TIME_WINDOW_LENGTH));
	    }
	    epService.getEPRuntime().sendEvent(cdr);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(BaseDistributedCountBolt.DEFAULT_OUTPUT_FIELDS);
	}

	@Override
	public void update(EventBean[] newEvents, EventBean[] oldEvents) {
		if (newEvents != null)
			for (EventBean event : newEvents) {
				String phone = (String) event.get("phone");
				int count = ((Long) event.get("count(*)")).intValue();
				// Need to make a complicated transform since I couldn't figure out how to get 
				// the last event's timestamp
				long totalTime = (long) event.get("sum(timestamp)");
				long timestamp = (long) (Math.floor((totalTime / count) / TIME_WINDOW_LENGTH)) * TIME_WINDOW_LENGTH;
				Message message = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, count);
				logger.info("Emitting counter update: {}", message);
				collector.emit(message.toDataTuple().asList());
			}
	}

}
