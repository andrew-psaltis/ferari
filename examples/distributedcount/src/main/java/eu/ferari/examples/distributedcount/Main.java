package eu.ferari.examples.distributedcount;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import com.google.gson.Gson;
import backtype.storm.utils.Utils;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.distributedcount.misc.Coordinator;
import eu.ferari.examples.distributedcount.misc.JedisConfiguration;
import eu.ferari.examples.distributedcount.storm.DistributedCountTopology;

public class Main {
	private static final Logger logger = LoggerFactory.getLogger(Main.class);

	private static final Map<String, String> propertiesToEvents = new HashMap<String, String>() {
		{
			put("pop/properties/36843.properties", "pop/events/36843.csv");
			put("pop/properties/36710.properties", "pop/events/36710.csv");
			put("pop/properties/36711.properties", "pop/events/36711.csv");
			put("pop/properties/36713.properties", "pop/events/36713.csv");
			put("pop/properties/36708.properties", "pop/events/36708.csv");
		}
	};
	private static final DistributedCountTopology[] topologies = new DistributedCountTopology[propertiesToEvents
			.size()];

	public static void main(String[] args) throws IOException {
		final JedisPoolConfig poolConfig = new JedisPoolConfig();
		JedisConfiguration jedisConfig = new JedisConfiguration();		
		try (JedisPool jedisPool = new JedisPool(poolConfig, jedisConfig.getHost(), jedisConfig.getPort(), jedisConfig.getTimeout())){
			Jedis publisherJedis = jedisPool.getResource();		
			ISend violationReporter = new Sender(publisherJedis, "violations");
			int i = 0;
			for (Map.Entry<String, String> entry : propertiesToEvents.entrySet()) {
				DistributedCountTopology topology = new DistributedCountTopology(
						entry.getKey(), entry.getValue());
				topologies[i++] = topology;
			}
			String propertyFile = propertiesToEvents.keySet().toArray(new String[0])[0];
			Coordinator coordinator = new Coordinator(propertyFile, violationReporter);

			coordinator.start();
			for (DistributedCountTopology topology : topologies)
				topology.start();

			Utils.sleep(100000);

			coordinator.stop();
			for (DistributedCountTopology topology : topologies)
				topology.stop();
	
		}
		
	}
	
	// TODO: consolidate all Jedis classes
	public static class Sender implements ISend {
		
		private final Jedis publisherJedis;
	    private final String channel;
	    private final Gson gson = new Gson();
	    
	    public Sender(Jedis publisherJedis, String channel) {
	        this.publisherJedis = publisherJedis;
	        this.channel = channel;
	    }
	    
		@Override
		public void signal(DataTuple data) {
			String message = gson.toJson(data);
			publisherJedis.publish(channel, message);
		}
		
	}
}
