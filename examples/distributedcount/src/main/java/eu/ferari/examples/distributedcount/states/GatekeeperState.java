package eu.ferari.examples.distributedcount.states;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.ferari.core.DataTuple;
import eu.ferari.examples.distributedcount.misc.Message;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.IState;

public class GatekeeperState implements IState {
	private static final Logger logger = LoggerFactory.getLogger(GatekeeperState.class);
	
	private final int defaultThreshold;
	
	private ISend sendToCommunicator;
	private Map<String, Integer> thresholds = new HashMap<String, Integer>();
	
	public GatekeeperState(ISend sendToCommunicator, int defaultThreshold){
		this.sendToCommunicator = sendToCommunicator;
		this.defaultThreshold = defaultThreshold;
		logger.info("Gatekeeper's default threshold is {}", defaultThreshold);
	}

	@Override
	public void update(DataTuple data) {
		Message message = new Message(data);		
		
		switch (message.getType()) {
		case CounterUpdate:
			if (message.getCounter() > getThreshold(message.getPhone())){
				logger.info("Passed threshold {} for phone {} with counter {} at timestamp {}",
						new Object[]{
						getThreshold(message.getPhone()),
						message.getPhone(),
						message.getCounter(),
						message.getTimestamp()
				});
				sendToCommunicator.signal(message.toDataTuple());
			} else {
				logger.debug("Phone {} didn't pass threshold {} at timestamp {} with counter {}",
						new Object[]{
						message.getPhone(),
						getThreshold(message.getPhone()),
						message.getTimestamp(),						
						message.getCounter()						
				});
			}			
			break;
		case NewThreshold:						
			logger.info("Updating threshold for phone {} to {}", message.getPhone(), message.getNewThreshold());
			thresholds.put(message.getPhone(), message.getNewThreshold());
			break;
		default:
			logger.error("Received message of type {} in gatekeeper", message.getType());
		}
	}
	
	public int getThreshold(String phone){
		return (thresholds.containsKey(phone) ? thresholds.get(phone) : defaultThreshold);
	}
	
	public int getDefaultThreshold(){
		return defaultThreshold;
	}
}
