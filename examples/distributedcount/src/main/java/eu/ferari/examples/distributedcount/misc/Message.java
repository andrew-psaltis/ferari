package eu.ferari.examples.distributedcount.misc;

import com.google.gson.Gson;

import eu.ferari.core.DataTuple;

public class Message {
	public enum MessageType { 
		CounterUpdate,
		CounterRequest,
		UpdateThreshold, 
		NewThreshold, 		
		Play,
		Pause,
		PauseConfirmation
	}
	
	private MessageType type;
	private String nodeId;
	private String phone;
	private long timestamp;
	private int counter;
	private int newThreshold;	
	
	/**
	 * Constructor for Counter Update, Pause Confirmation, Update Threshold message
	 * @param type
	 * @param nodeId
	 * @param phone
	 * @param timestamp
	 * @param counter
	 */
	public Message(MessageType type, String nodeId, String phone, long timestamp, int counter){
		this(type, nodeId, phone);
		
		switch (type) {
		case CounterUpdate:
		case PauseConfirmation:
			this.counter = counter;
			break;
		case UpdateThreshold:
			this.newThreshold = counter;
			break;
		default:
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		}
		this.timestamp = timestamp;		
	}
	
	/**
	 * Constructor for Counter Request, Pause, Play message
	 * @param type
	 * @param nodeId
	 * @param phone
	 * @param timestamp
	 */
	public Message(MessageType type, String nodeId, String phone, long timestamp){
		this(type, nodeId, phone);
		switch (type) {
		case CounterRequest:
		case Pause:
		case Play:
			break;
		default:
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		}
		this.timestamp = timestamp;
	}
	
	/**
	 * Constructor for New Threshold message
	 * @param type
	 * @param nodeId
	 * @param phone
	 * @param newThreshold
	 */
	public Message(MessageType type, String nodeId, String phone, int newThreshold){
		this(type, nodeId, phone);
		if (!type.equals(MessageType.NewThreshold))
			throw new IllegalArgumentException("Can't handle message type " + type + " in this constructor");
		this.newThreshold = newThreshold;
	}
	
	private Message(MessageType type, String nodeId, String phone){
		this.type = type;
		this.nodeId = nodeId;
		this.phone = phone;
	}
	
	public Message(DataTuple message){
		this(MessageType.valueOf(message.getString(0)), message.getString(1), message.getString(2));
		switch (type) {
		case CounterUpdate:
		case PauseConfirmation:		
			this.counter = message.getInteger(3);
			this.timestamp = message.getLong(4);
			break;
		case UpdateThreshold:
			this.newThreshold = message.getInteger(3);
			this.timestamp = message.getLong(4);
			break;
		case CounterRequest:
		case Pause:
		case Play:
			this.timestamp = message.getLong(3);
			break;
		case NewThreshold:
			this.newThreshold = message.getInteger(3);
			break;
		default:
			throw new IllegalArgumentException("Unknown message type " + type);
		}	
	}
	
	public DataTuple toDataTuple(){
		DataTuple message = new DataTuple(String.valueOf(type), nodeId, phone);
		switch (type) {
		case CounterUpdate:
		case PauseConfirmation:		
			message.add(counter, timestamp);
			break;
		case UpdateThreshold:
			message.add(newThreshold, timestamp);
			break;
		case CounterRequest:
		case Pause:
		case Play:
			message.add(timestamp);
			break;
		case NewThreshold:
			message.add(newThreshold);
			break;
		default:
			throw new IllegalArgumentException("Unknown message type " + type);
		}
		
		return message;
	}
	
	public Message toPauseMessage(String nodeId){
		return new Message(MessageType.Pause, nodeId, phone, timestamp);
	}
	
	public MessageType getType() {
		return type;
	}

	public String getNodeId() {
		return nodeId;
	}

	public String getPhone() {
		return phone;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public int getCounter() {
		return counter;
	}

	public int getNewThreshold() {
		return newThreshold;
	}
	
	public String toJSON(){
		Gson gson = new Gson();
		
		return gson.toJson(this);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Type: " + type);
		sb.append(", NodeId: " + nodeId);
		sb.append(", Phone: " + phone);
		
		switch (type) {		
		case CounterUpdate:
		case PauseConfirmation:
			sb.append(", Timestamp: " + timestamp);
			sb.append(", Counter: " + counter);			
			break;		
		case UpdateThreshold:
			sb.append(", Timestamp: " + timestamp);
			sb.append(", New Threshold: " + newThreshold);
			break;
		case NewThreshold:
			sb.append(", New Threshold: " + newThreshold);
			break;
		case CounterRequest:
		case Play:
		case Pause:
			sb.append(", Timestamp: " + timestamp);
			break;				
		default:
			throw new IllegalArgumentException("Don't know how to handle message of type: " + type);
		}
		
		return sb.toString();
	}
}
