package eu.ferari.examples.distributedcount.storm.senders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

public class DirectSender implements ISend {
	private static final Logger logger = LoggerFactory.getLogger(DirectSender.class);
	
	private OutputCollector collector;
	private TopologyContext context;
	private int targetTaskId;
	private String streamId;
	
	public DirectSender(OutputCollector collector, TopologyContext context, 
			String targetComponentName, String streamId) {
		this.collector = collector;
		this.context = context;		
		targetTaskId = context.getComponentTasks(targetComponentName).get(context.getThisTaskIndex());
		logger.trace("Set target task ID: {}, this task index: {}", targetTaskId, context.getThisTaskIndex());
		this.streamId = streamId;
	}

	@Override
	public void signal(DataTuple data) {		
		logger.info("Emitting to {} on stream {}, task index: {}",
				new Object[]{
					context.getComponentId(targetTaskId), streamId, context.getThisTaskIndex()
				});
		collector.emitDirect(targetTaskId, streamId, data.asList());
	}

}
