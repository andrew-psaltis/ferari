package eu.ferari.examples.distributedcount.storm.senders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.task.OutputCollector;
import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

public class StreamSender implements ISend {
	private static final Logger logger = LoggerFactory.getLogger(StreamSender.class);
	
	private final OutputCollector collector;
	private final String streamId;
	
	public StreamSender(OutputCollector collector, String streamId) {
		 this.collector = collector;
		 this.streamId = streamId;
	}

	@Override
	public void signal(DataTuple data) {
		logger.info("Emitting on stream {}", streamId);
		collector.emit(streamId, data.asList());
	}

}
