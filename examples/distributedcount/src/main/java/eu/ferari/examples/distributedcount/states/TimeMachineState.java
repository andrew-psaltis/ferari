package eu.ferari.examples.distributedcount.states;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.IState;
import eu.ferari.examples.distributedcount.misc.DefaultHashMap;
import eu.ferari.examples.distributedcount.misc.LimitedSizeList;
import eu.ferari.examples.distributedcount.misc.LimitedSizeOrderedSet;
import eu.ferari.examples.distributedcount.misc.Message;
import eu.ferari.examples.distributedcount.misc.Message.MessageType;
import eu.ferari.examples.distributedcount.misc.TimeWindowCount;

public class TimeMachineState implements IState  {
	private static final Logger logger = LoggerFactory.getLogger(TimeMachineState.class);
	
	private final int BUFFER_SIZE = 100;
	
	public enum PhoneState { Pause, Play }
	private ISend sendToGatekeeper;
	private ISend sendToCommunicator;		
	private Map<String, LimitedSizeOrderedSet<TimeWindowCount>> data = new DefaultHashMap<String, LimitedSizeOrderedSet<TimeWindowCount>>(new LimitedSizeOrderedSet<TimeWindowCount>(BUFFER_SIZE));
	private Map<String, PhoneState> phoneStates = new DefaultHashMap<String, PhoneState>(PhoneState.Play);
	
	public TimeMachineState(ISend sendToGatekeeper, ISend sendToCommunicator) {
		this.sendToGatekeeper= sendToGatekeeper;
		this.sendToCommunicator = sendToCommunicator;
	}

	@Override
	public void update(DataTuple tuple) {
		Message message = new Message(tuple);
		String phone = message.getPhone();
		
		if (!phoneStates.containsKey(phone))
			initializePhone(phone);
		
		TimeWindowCount timeWindow = 
				new TimeWindowCount(message.getCounter(), message.getTimestamp());
		
		switch (message.getType()) {
		case CounterUpdate:	
			logger.info("Counter update received: {}", message);
			data.get(phone).add(timeWindow);
			if (isPlaying(phone))
				sendToGatekeeper.signal(tuple);											
			break;
		case Pause:		
			logger.info("Pausing phone {}", phone);
			phoneStates.put(phone, PhoneState.Pause);
			TimeWindowCount requestedTimeWindow = data.get(phone).get(timeWindow);
			Message pauseConfirmationMessage;
			if (requestedTimeWindow != null){
				pauseConfirmationMessage = new Message(
						MessageType.PauseConfirmation, message.getNodeId(), 
						phone, message.getTimestamp(), requestedTimeWindow.getCount()
					);									
			} else {
				pauseConfirmationMessage = new Message(
						MessageType.PauseConfirmation, message.getNodeId(), 
						phone, message.getTimestamp(), 0
					);
				logger.info("Couldn't find time window for phone {} at timestamp {}", phone, message.getTimestamp());
			}	
			logger.info("Emitting pause confirmation: {}", pauseConfirmationMessage);
			sendToCommunicator.signal(pauseConfirmationMessage.toDataTuple());
			break;	
		case Play:			
			logger.info("Playing phone {}", phone);
			phoneStates.put(phone, PhoneState.Play);
			for (TimeWindowCount currentTimeWindow : data.get(phone).rewind(timeWindow)) {				
				Message counterUpdateMessage = new Message(
						MessageType.CounterUpdate,
						message.getNodeId(), 
						phone, 
						currentTimeWindow.getTimestamp(), 
						currentTimeWindow.getCount()
					);
				logger.debug("Playing message {}", counterUpdateMessage);
				sendToGatekeeper.signal(counterUpdateMessage.toDataTuple());
			}
			break;
		default:
			logger.error("Don't know how to handle message of type {}", message.getType());
			break;
		}
	}
	
	public PhoneState getState(String phone){
		return phoneStates.get(phone);
	}
	
	private void initializePhone(String phone){
		logger.debug("Received new phone {}", phone);
//		phoneStates.put(phone, PhoneState.Play);
//		data.put(phone, new LimitedSizeOrderedSet<TimeWindowCount>(BUFFER_SIZE));
	}
	
	private boolean isPlaying(String phone){
		return getState(phone) == PhoneState.Play;
	}				
}
