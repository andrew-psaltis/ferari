package eu.ferari.examples.distributedcount.bare.senders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.IState;

public class SimpleSender implements ISend {
	private static final Logger logger = LoggerFactory.getLogger(SimpleSender.class);
	private IState target;

	@Override
	public void signal(DataTuple data) {
		if (target == null){
			logger.error("Target has not been initialized");
			return; // TODO: better to throw exception, but then have change signature of this method
		}
			
		logger.info("Transferring message of type {} to {}", data.getString(0), target.getClass().getSimpleName());
		target.update(data);
	}
	
	public void setTarget(IState target){
		this.target = target;
	}
}
