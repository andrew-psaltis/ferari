package eu.ferari.examples.distributedcount.bare.senders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.distributedcount.misc.Message;
import redis.clients.jedis.Jedis;

public class RedisSender implements ISend {
	private static final Logger logger = LoggerFactory.getLogger(RedisSender.class);
	private final Jedis publisherJedis;

    private final String channel;

    public RedisSender(Jedis publisherJedis, String channel) {
        this.publisherJedis = publisherJedis;
        this.channel = channel;
        logger.info("Redis sender ready");
    }

	@Override
	public void signal(DataTuple data) {		
		Message message = new Message(data);
		String json = message.toJSON();
		logger.info("Redis sender sending {}", json);
		publisherJedis.publish(channel, json);
	}
	
}
