package eu.ferari.examples.distributedcount.states;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.core.interfaces.IState;
import eu.ferari.examples.distributedcount.exceptions.NodeDataAlreadyExistsException;
import eu.ferari.examples.distributedcount.exceptions.NodeDataNotYetCompleteException;
import eu.ferari.examples.distributedcount.exceptions.NodeListCompleteException;
import eu.ferari.examples.distributedcount.misc.Message;
import eu.ferari.examples.distributedcount.misc.Message.MessageType;
import eu.ferari.examples.distributedcount.misc.Violation;

public class CoordinatorState implements IState {
	private static final Logger logger = LoggerFactory.getLogger(CoordinatorState.class);
			
	private final ISend sendCounterRequest;
	private final ISend sendThresholdUpdate;
	private final int numNodes;
	private final int globalThreshold;
	private final Map<String, Violation> violations = new HashMap<String, Violation>();
	private final Set<String> nodeIds = new HashSet<String>();
	private ISend violationReporter;	
	
	public CoordinatorState(ISend sendCounterRequest, ISend sendThresholdUpdate, int numNodes, int globalThreshold) {
		this.sendCounterRequest = sendCounterRequest;
		this.sendThresholdUpdate = sendThresholdUpdate;
		this.numNodes = numNodes;
		this.globalThreshold = globalThreshold;
	}

	@Override
	public void update(DataTuple data) {
		Message thresholdPassMessage = new Message(data);
		if (!thresholdPassMessage.getType().equals(MessageType.CounterUpdate)){
			logger.error("Received unexpected message type: {}", thresholdPassMessage);
			return;
		}
		
		logger.info("Received message {}", thresholdPassMessage);
		
		nodeIds.add(thresholdPassMessage.getNodeId());
		
		if (!violations.containsKey(thresholdPassMessage.getPhone()))
			beginViolationResolution(thresholdPassMessage);								
		
		Violation violation = violations.get(thresholdPassMessage.getPhone());
		
		if (violation.getTimestamp() < thresholdPassMessage.getTimestamp()){
			logger.info("Violation's timestamp {} is lower than message's {}, will ignore message",
					violation.getTimestamp(), thresholdPassMessage.getTimestamp());
			return;
		}
		
		try {
			boolean isComplete = violation.add(thresholdPassMessage);
			if (isComplete){
				logger.info("Violation resolution complete. {}", violation);
				if (violationReporter != null && violation.getGlobalCount() >= violation.getGlobalThreshold()){
					DataTuple tuple = new DataTuple(violation.getPhone(), violation.getTimestamp(), violation.getGlobalCount());
					logger.info("Reporting global violation. Phone, {}, Timestamp: {}, Count: {}", 
							violation.getPhone(), violation.getTimestamp(), violation.getGlobalCount());
					violationReporter.signal(tuple);
				}
					
				for (String nodeId : nodeIds) {
					try {
						int newThreshold = violation.getNewThreshold(nodeId);
						Message updateThresholdMessage = new Message(MessageType.UpdateThreshold, nodeId, violation.getPhone(), violation.getTimestamp(), newThreshold);
						sendThresholdUpdate.signal(updateThresholdMessage.toDataTuple());
					} catch (NodeDataNotYetCompleteException e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}
				}
				violations.remove(violation.getPhone());
			} else if (violation.getNumReportedNodes() == 1) { // We ask for other nodes' counters when the first node is reported				
				Message counterRequestMessage = new Message(
						MessageType.CounterRequest, 
						thresholdPassMessage.getNodeId(), 
						thresholdPassMessage.getPhone(), 
						thresholdPassMessage.getTimestamp()
					);
				sendCounterRequest.signal(counterRequestMessage.toDataTuple());
			}
		} catch (NodeDataAlreadyExistsException e) {
			logger.error("{}, {}", e.getMessage(), e);
		} catch (NodeListCompleteException e1) {
			logger.error("{}, {}", e1.getMessage(), e1);
		}
	}	
	
	public void setViolationReporter(ISend violationReporter){
		this.violationReporter = violationReporter;
	}
	
	private void beginViolationResolution(Message thresholdPassMessage){
		logger.debug("Begin violation resolution for phone {} at timestamp {} from node {}", 
				thresholdPassMessage.getPhone(), thresholdPassMessage.getTimestamp(), thresholdPassMessage.getNodeId());
		violations.put(thresholdPassMessage.getPhone(), new Violation(thresholdPassMessage, numNodes, globalThreshold));
	}
}
