package eu.ferari.examples.distributedcount.exceptions;

public class UnknownTypeException extends Exception {

	public UnknownTypeException(String type) {
		super("Don't know how to handle type " + type);
	}	

}
