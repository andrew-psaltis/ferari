package eu.ferari.examples.distributedcount.misc;

/**
 * Counter data for a single time window
 * @author yoni
 *
 */
public class TimeWindowCount implements Comparable<TimeWindowCount> {
	private int count;
	private long timestamp;				
	
	public TimeWindowCount(int count, long timestamp) {
		this.count = count;
		this.timestamp = timestamp;
	}
	
	public int getCount() {
		return count;
	}
	public long getTimestamp() {
		return timestamp;
	}	
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof TimeWindowCount))
			return false;
		
		return getTimestamp() == ((TimeWindowCount) obj).getTimestamp(); 			
	}
	
	@Override
	public int hashCode() {
		return Long.valueOf(timestamp).hashCode();
	}

	@Override
	public int compareTo(TimeWindowCount other) {		
		if (this.getTimestamp() < other.getTimestamp()){
			return -1;
		} else if (this.getTimestamp() == other.getTimestamp()){
			return 0;
		} else {
			return 1;
		}
	}
}
