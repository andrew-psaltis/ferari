package eu.ferari.examples.distributedcount.storm.bolts;

import java.util.Map;

import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.distributedcount.states.GatekeeperState;
import eu.ferari.examples.distributedcount.storm.senders.DirectSender;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;

public class GatekeeperBolt extends BaseDistributedCountBolt {
	private final String communicatorName;
	private final String toCommunicatorStreamId;
	private final int defaultThreshold;
	
	public GatekeeperBolt(String communicatorName, String toCommunicatorStreamId, int defaultThreshold) {
		this.communicatorName = communicatorName;
		this.toCommunicatorStreamId = toCommunicatorStreamId;
		this.defaultThreshold = defaultThreshold;
	}

	@Override
	public void prepare(Map conf, TopologyContext context,
			OutputCollector collector) {
		super.prepare(conf, context, collector);
		ISend sendToCommunicator = new DirectSender(collector, context, communicatorName, toCommunicatorStreamId);
		this.state = new GatekeeperState(sendToCommunicator, defaultThreshold);		
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(toCommunicatorStreamId, true, DEFAULT_OUTPUT_FIELDS);		
	}
}
