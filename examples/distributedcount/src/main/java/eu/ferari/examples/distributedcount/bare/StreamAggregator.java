package eu.ferari.examples.distributedcount.bare;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;

import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.distributedcount.misc.Message;
import eu.ferari.examples.distributedcount.misc.Message.MessageType;

public class StreamAggregator {
	private static final Logger logger = LoggerFactory.getLogger(StreamAggregator.class);
	private final CdrFromFileProducer cdrProducer;
	private final EPServiceProvider epService = EPServiceProviderManager.getDefaultProvider();
	private final String expression = "select nodeId, phone, count(*), timestamp from eu.ferari.examples.distributedcount.bare.Cdr.win:ext_timed(timestamp, 10 sec) group by nodeId, phone";
	private final EPStatement statement = epService.getEPAdministrator().createEPL(expression);
	private final CdrListener cdrListener;
	
	public StreamAggregator(String filepath, ISend sender) throws IOException {
		cdrProducer = new CdrFromFileProducer(filepath);
		cdrListener = new CdrListener(sender);
		statement.addListener(cdrListener);
		logger.info("Stream aggregator ready");
	}

	public void produce(){
		while (cdrProducer.hasNext()){
			logger.info("Emitting CDR");
			epService.getEPRuntime().sendEvent(cdrProducer.next());
		}
	}
	
	public class CdrListener implements UpdateListener {
		private final ISend sender;
		
		public CdrListener(ISend sender){
			this.sender = sender;
		}
		
		@Override
		public void update(EventBean[] newEvents, EventBean[] oldEvents) {
			EventBean event = newEvents[0];
			String nodeId = (String) event.get("nodeId");
			String phone = (String) event.get("phone");
			long timestamp = (Long) event.get("timestamp");
			int count = (Integer) event.get("count(*)");			
			Message cdrMessage = new Message(MessageType.CounterUpdate, nodeId, phone, timestamp, count);
			logger.info("Emitting data for window: {}", cdrMessage);
			sender.signal(cdrMessage.toDataTuple());
		}		
	}
}
