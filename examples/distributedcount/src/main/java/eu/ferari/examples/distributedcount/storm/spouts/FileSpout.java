package eu.ferari.examples.distributedcount.storm.spouts;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

public class FileSpout extends BaseRichSpout {

	private static final long serialVersionUID = -7995618908649605861L;

	private static final Logger logger = LoggerFactory.getLogger(FileSpout.class);
	
	private final SimpleDateFormat dateParser; 
	
	private final int phoneIndex;
	private final int timestampIndex;
	private final int nodeIdIndex;
	
	private final File file;
	private BufferedReader reader;
	private SpoutOutputCollector collector;
	
	public FileSpout(String filepath, int phoneIndex, int nodeIdIndex, int timestampIndex, String timestampFormat) {
		file = new File(filepath);
		this.phoneIndex = phoneIndex;
		this.nodeIdIndex = nodeIdIndex;
		this.timestampIndex = timestampIndex;
		this.dateParser = (timestampFormat == null ? null : new SimpleDateFormat(timestampFormat));
	}	

	@Override
	public void nextTuple() {
		try {
			for(String line; (line = reader.readLine()) != null; ) {
			    String[] parts = line.split(",");
			    
			    try {
					String phone = parts[phoneIndex];
					String nodeId = parts[nodeIdIndex];
					
					if (nodeId.contains(".")){
						int nodeIdInteger = Integer.valueOf(nodeId.split("\\.")[0]);
						nodeId = String.valueOf(nodeIdInteger);
					}
					
					long timestamp;
					if (dateParser == null){
						timestamp = Long.valueOf(parts[timestampIndex]);
					} else {
						Date date = dateParser.parse(parts[timestampIndex]);
						timestamp = date.getTime();
					}
					Values values = new Values(nodeId, phone, timestamp);
					
					logger.debug("Read line. Phone: {}, NodeId: {}, Timestamp: {}", phone, nodeId, timestamp);	
					collector.emit(values, UUID.randomUUID());
				} catch (NumberFormatException | ParseException e) {
					logger.error("Error parsing timestamp", e);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void open(Map config, TopologyContext context, SpoutOutputCollector collector) {
		this.collector = collector;
		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("nodeId", "phone", "timestamp"));
	}
	
	@Override
	public void ack(Object arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void fail(Object arg0) {
		// TODO Auto-generated method stub
	}
}
