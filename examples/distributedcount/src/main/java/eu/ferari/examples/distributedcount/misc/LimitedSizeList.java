package eu.ferari.examples.distributedcount.misc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Deprecated
public class LimitedSizeList<T extends Comparable<T>> implements Iterable<T> {
	private List<T> list = new ArrayList<T>();
    private int maxSize;
    private int currentPosition;

    public LimitedSizeList(int maxSize){
        this.maxSize = maxSize;
    }
    
    // TODO: throw exception if list is not ordered
	public boolean add(T element) {
		if (list.size() == maxSize)
			list.remove(0);
		
		return list.add(element);
	}
	
	/**
	 * Returns an element equal to the request element.
	 * @param element
	 * @return Element equal to the request element.
	 */
	public T getAt(T element){
		int index = list.indexOf(element);
		
		return (index == -1 ? null : list.get(index));
	}
	
	/** Rewind iterator to the position of search element or the 
	 * not found.
	 * @param searchElement
	 * @return True if element was found.
	 */
	public boolean rewind(T searchElement){
		for (int i = 0; i < list.size(); i++) {
			if (searchElement.compareTo(list.get(i)) > -1){
				currentPosition = i;
				return true;
			}			
		}			
		
		return false;
	}		
	
	public int size(){
		return list.size();
	}
	
	public T get(int index){
		return list.get(index);
	}
	
	@Override
	public Iterator<T> iterator() {
		Iterator<T> it = new Iterator<T>() {
			@Override
			public boolean hasNext() {
				return currentPosition < list.size();
			}

			@Override
			public T next() {
				return list.get(currentPosition++);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}

		};
		
		return it;
	}	
}
