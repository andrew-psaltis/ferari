package eu.ferari.examples.distributedcount.storm.spouts;

import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import eu.ferari.examples.distributedcount.misc.JedisConfiguration;
import eu.ferari.examples.distributedcount.misc.RedisListenerThread;
import eu.ferari.examples.distributedcount.misc.Message;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.utils.Utils;

/**
 * Stolen from
 * https://github.com/stormprocessor/storm-redis-pubsub/blob/master/src
 * /jvm/yieldbot/storm/spout/RedisPubSubSpout.java
 *
 */
public class RedisPubSubSpout extends BaseRichSpout {

	static final long serialVersionUID = 737015313288609460L;
	private static final Logger logger = LoggerFactory.getLogger(RedisPubSubSpout.class);

	private SpoutOutputCollector _collector;
	private LinkedBlockingQueue<String> queue;
	private JedisPool jedisPool;
	private final String channel;
	private final String counterRequestStream;
	private final String thresholdUpdateStream;
	private Gson gson;

	public RedisPubSubSpout(String redisChannel, String counterRequestStream, String thresholdUpdateStream) {
		this.channel = redisChannel;
		this.counterRequestStream = counterRequestStream;
		this.thresholdUpdateStream = thresholdUpdateStream;
	}	

	public void open(Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		gson = new Gson();
		final JedisPoolConfig poolConfig = new JedisPoolConfig();
		JedisConfiguration jedisConfig = new JedisConfiguration();
		this.jedisPool = new JedisPool(poolConfig, jedisConfig.getHost(),
				jedisConfig.getPort(), jedisConfig.getTimeout());
		_collector = collector;
		queue = new LinkedBlockingQueue<String>(1000);

		RedisListenerThread listener = new RedisListenerThread(queue, jedisPool, channel);
		listener.start();
	}

	public void close() {
		jedisPool.close();
	}

	public void nextTuple() {
		String json = queue.poll();
		if (json == null) {
			Utils.sleep(50);
		} else {
			Message message = gson.fromJson(json, Message.class);
			switch (message.getType()) {
			case CounterRequest:
				_collector.emit(counterRequestStream, message.toDataTuple().asList());
				break;
			case UpdateThreshold:
				_collector.emit(thresholdUpdateStream, message.toDataTuple().asList());
				break;
			default:
				throw new IllegalArgumentException("Don't know how to handle message type " + message.getType());
			}			
		}
	}

	public void ack(Object msgId) {
		// TODO Auto-generated method stub

	}

	public void fail(Object msgId) {
		// TODO Auto-generated method stub

	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(counterRequestStream, new Fields("type", "nodeId", "phone", "timestamp"));
		declarer.declareStream(thresholdUpdateStream, new Fields("type", "nodeId", "phone", "timestamp", "counter"));
	}

	public boolean isDistributed() {
		return false;
	}
}