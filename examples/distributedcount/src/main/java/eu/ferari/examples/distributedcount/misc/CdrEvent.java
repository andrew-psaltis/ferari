package eu.ferari.examples.distributedcount.misc;

import backtype.storm.tuple.Tuple;

public class CdrEvent {
	private final String phone;
	private final String nodeId;
	private final long timestamp;
	
	public CdrEvent(String phone, String nodeId, long timestamp) {
		this.phone = phone;
		this.nodeId = nodeId;
		this.timestamp = timestamp;
	}
	
	public CdrEvent(Tuple tuple){
		phone = tuple.getString(1);
		nodeId = tuple.getString(0);
		timestamp = tuple.getLong(2);
	}

	public String getPhone() {
		return phone;
	}

	public String getNodeId() {
		return nodeId;
	}

	public long getTimestamp() {
		return timestamp;
	}	
	
	public int getTimeWindow(long length){
		return (int) Math.floor(timestamp / length);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Phone: ");
		sb.append(phone);
		sb.append(", NodeId: ");
		sb.append(nodeId);
		sb.append(", Timestamp: ");
		sb.append(timestamp);
		
		return sb.toString();
	}
}
