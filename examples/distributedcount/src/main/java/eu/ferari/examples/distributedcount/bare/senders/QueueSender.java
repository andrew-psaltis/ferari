package eu.ferari.examples.distributedcount.bare.senders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ISend;

public class QueueSender implements ISend {
	private static final Logger logger = LoggerFactory.getLogger(QueueSender.class);
	
	public QueueSender() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void signal(DataTuple data) {
		logger.info("Putting method of type {} into queue", data.getString(0));
	}

}
