package eu.ferari.examples.distributedcount.misc;

import java.util.Collection;
import java.util.NavigableSet;
import java.util.TreeSet;

import org.apache.commons.lang.NotImplementedException;

public class LimitedSizeOrderedSet<T> extends TreeSet<T> {
	private int maxSize;
	
	public LimitedSizeOrderedSet(int maxSize){
		this.maxSize = maxSize;
	}
	
	@Override
	public boolean add(T e) {
		boolean returnValue = super.add(e);
		if (size() > maxSize)
			remove(first());
		
		return returnValue;
	}
	
	@Override
	public boolean addAll(Collection<? extends T> c) {
		throw new NotImplementedException();
	}
	
	public NavigableSet<T> rewind(T element){
		return tailSet(element, true);
	}
	
	public T get(T element){
		return (contains(element) ? floor(element) : null);
	}
}
