package eu.ferari.examples.distributedcount.misc;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import backtype.storm.Config;

public class Utils {

	public static Properties loadProperties(String filename, Object klass) throws IOException{
		Properties properties = new Properties();
		 
		InputStream inputStream = klass.getClass().getClassLoader().getResourceAsStream(filename);
		 
		if (inputStream != null) {
			properties.load(inputStream);
			return properties;
		} else {
			throw new FileNotFoundException("Property file '" + filename + "' not found in the classpath");
		}
	}
	
	public static Config propertiesToStormConfiguration(Properties properties){
		Config conf = new Config();
        conf.setDebug(false);
        for (Enumeration e = properties.keys(); e.hasMoreElements();){
        	String key = (String) e.nextElement();
        	conf.put(key, properties.get(key));
        }
        
        return conf;
	}
}
