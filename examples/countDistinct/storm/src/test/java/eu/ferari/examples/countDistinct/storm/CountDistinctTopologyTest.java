package eu.ferari.examples.countDistinct.storm;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.BoltDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.utils.Utils;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.backend.storm.bolts.CoordinatorBolt;
import eu.ferari.backend.storm.bolts.FileWriterBolt;
import eu.ferari.backend.storm.bolts.LocalBolt;
import eu.ferari.backend.storm.spouts.FileLineReaderSpout;
import eu.ferari.examples.countDistinct.common.CoordinatorCountDistinctState;
import eu.ferari.examples.countDistinct.common.LocalCountDistinctState;
import eu.ferari.examples.countDistinct.common.countDistinctConstants;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.Assert.*;

public class CountDistinctTopologyTest {

    private static final String SPOUT_ONE_ID = "1";
    private static final Logger LOG = LoggerFactory.getLogger(CountDistinctTopology.class);

    private final String filePath=this.getClass().getClassLoader().getResource("sorted_sample.tsv").getPath();

@Test
public void countDistinctTopologyTest() throws InterruptedException, AlreadyAliveException,
        InvalidTopologyException, IOException {

        // Config settings
        Config conf = new Config();
        conf.setDebug(false);

        CountDistinctTopologyTest test=new CountDistinctTopologyTest();


        conf.put("INFILE",test.filePath);

        // Build the topology
        TopologyBuilder builder = new TopologyBuilder();

        builder.setSpout(SPOUT_ONE_ID, new FileLineReaderSpout(), 1);

        builder.setBolt("preprocBolt", new PreprocBolt(), 1).shuffleGrouping(SPOUT_ONE_ID);

        BoltDeclarer localBolt =
            builder.setBolt("localBolt", new LocalBolt(new LocalCountDistinctState()),
                countDistinctConstants.NUM_PARRALELL_OPS);

        localBolt.shuffleGrouping("preprocBolt"); //better load distribution,

        BoltDeclarer coordinatorBolt =
            builder.setBolt("coordinatorBolt", new CoordinatorBolt(new CoordinatorCountDistinctState()));

        coordinatorBolt.globalGrouping("localBolt", StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME);

        localBolt.allGrouping("coordinatorBolt", StormSendToLocal.TO_LOCAL_STREAM_NAME);

        builder.setBolt("fileWriter", new FileWriterBolt("topologyOut.tsv", 1),1).allGrouping("localBolt", StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME);

        conf.setMaxTaskParallelism(countDistinctConstants.NUM_PARRALELL_OPS);

        LocalCluster cluster = new LocalCluster();

        cluster.submitTopology("test", conf, builder.createTopology());
        LOG.info("Topology submitted locally");

        Utils.sleep(10000);
        cluster.shutdown();

        int i = 0;
    BufferedReader reader = new BufferedReader(new FileReader("topologyOut.tsv"));
    while(reader.readLine() != null) ++i;
    reader.close();

    assertNotEquals(0, i);
    File file = new File("topologyOut.tsv");
    file.delete();

    }
}