package eu.ferari.examples.countDistinct.storm;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.BoltDeclarer;
import backtype.storm.topology.TopologyBuilder;
import eu.ferari.backend.storm.StormSendToCoordinator;
import eu.ferari.backend.storm.StormSendToLocal;
import eu.ferari.backend.storm.bolts.CoordinatorBolt;
import eu.ferari.backend.storm.bolts.LocalBolt;
import eu.ferari.backend.storm.spouts.FileLineReaderSpout;
import eu.ferari.examples.countDistinct.common.CoordinatorCountDistinctState;
import eu.ferari.examples.countDistinct.common.countDistinctConstants;
import eu.ferari.examples.countDistinct.common.LocalCountDistinctState;

/**
 * Example distinct count of events in stream.
 * <p>
 * The example shows the storm topology, generating distinct counts for the
 * music dataset. It determines the distinct number of hear events for a
 * artists.
 */
public class CountDistinctTopology {

    private static final String SPOUT_ONE_ID = "1";
    private static final Logger LOG = LoggerFactory.getLogger(CountDistinctTopology.class);

    public static void main(String[] args) throws InterruptedException, AlreadyAliveException,
            InvalidTopologyException, IOException {

        // Config settings
        Config conf = new Config();
        conf.setDebug(false);

        conf.put("INFILE", countDistinctConstants.INFILENAME);

        // Build the topology
        TopologyBuilder builder = new TopologyBuilder();

        builder.setSpout(SPOUT_ONE_ID, new FileLineReaderSpout(), 1);

        builder.setBolt("preprocBolt", new PreprocBolt(), 1).shuffleGrouping(SPOUT_ONE_ID);

        BoltDeclarer localBolt =
            builder.setBolt("localBolt", new LocalBolt(new LocalCountDistinctState()),
                countDistinctConstants.NUM_PARRALELL_OPS);

        localBolt.shuffleGrouping("preprocBolt"); //better load distribution,

        BoltDeclarer coordinatorBolt =
            builder.setBolt("coordinatorBolt", new CoordinatorBolt(new CoordinatorCountDistinctState()));

        coordinatorBolt.globalGrouping("localBolt", StormSendToCoordinator.TO_COORDINATOR_STREAM_NAME);

        localBolt.allGrouping("coordinatorBolt", StormSendToLocal.TO_LOCAL_STREAM_NAME);

        if (args != null && args.length == 3) {

            conf.setNumWorkers(countDistinctConstants.NUM_PARRALELL_OPS);

            StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
            LOG.info("Topology submitted");
        } else {
            conf.setMaxTaskParallelism(countDistinctConstants.NUM_PARRALELL_OPS);

            LocalCluster cluster = new LocalCluster();

            cluster.submitTopology("test", conf, builder.createTopology());
            LOG.info("Topology submitted locally");

        }
    }
}
