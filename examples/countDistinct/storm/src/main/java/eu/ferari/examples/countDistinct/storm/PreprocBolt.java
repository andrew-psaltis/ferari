package eu.ferari.examples.countDistinct.storm;

import java.util.Map;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import eu.ferari.core.DataTuple;
import eu.ferari.examples.countDistinct.common.ParseLineToTupleDetector;

/**
 * Preprocessing bolt.
 * <p>
 * Converts a raw text line to a Storm tuple.
 */
public class PreprocBolt implements IRichBolt {

    private static final long serialVersionUID = 1L;
    private OutputCollector collector;

    @Override
    public void prepare(@SuppressWarnings("rawtypes") Map conf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
    }

    @Override
    public void execute(Tuple tuple) {

        // Grab the input line
        String text = tuple.getString(0);
        DataTuple processedTuple = ParseLineToTupleDetector.process(new DataTuple(text));

        if (processedTuple != null) {
            // Collect values, first String is artist, second object is DataTuple with complete content
            Values values = new Values();
            values.add(processedTuple.getString(0));
            values.add(processedTuple.getValue(1));

            this.collector.emit(values);
        }
        this.collector.ack(tuple);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("artid", "datarecord"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }

    @Override
    public void cleanup() {
        this.collector = null;
    }
}
