package eu.ferari.examples.countDistinct.common.test;

import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import eu.ferari.examples.countDistinct.common.countdistinct.CountDistinctContainer;

public class CountDistinctContainerTest {

    private Date sinceWhenDate;

    @Before
    public void setup() {
        Calendar cal = Calendar.getInstance();
        cal.set(1970, Calendar.JANUARY, 1); //assume nothing happened before that
        sinceWhenDate = cal.getTime();
    }

    @Test
    public void testCountDistinctContainer() {
        // Need to extend Constructor to re-enable this choice if this should be a Runtime Decision at al
//        CountDistinctContainer container1 =
//            new CountDistinctContainer("Autreche", sinceWhenDate, CountDistinctSetBased.class);
//
//        Assert.assertNotNull(container1);

        CountDistinctContainer container2 = new CountDistinctContainer("Autreche", sinceWhenDate);
        Assert.assertNotNull(container2);

    }

    @Test
    public void testCountDistinctContainerAdd() {

        CountDistinctContainer c = new CountDistinctContainer("Autreche", sinceWhenDate);
        c.add(new Date(), "michael");

        Assert.assertTrue(c.isIncreased());
        c.add(new Date(), "michael");
        Assert.assertFalse(c.isIncreased());
        c.add(new Date(), "daniel");
        Assert.assertTrue(c.isIncreased());

    }

    @Test
    public void testCountDistinctContainerMerge() {

        CountDistinctContainer c1 = new CountDistinctContainer("Autreche", sinceWhenDate);
        CountDistinctContainer c2 = new CountDistinctContainer("Autreche", sinceWhenDate);
        c1.add(new Date(), "michael");
        c2.add(new Date(), "michael");
        c1.mergeWith(c2);

        Assert.assertFalse(c1.isIncreased());
        c2.add(new Date(), "daniel");
        c1.mergeWith(c2);
        Assert.assertTrue(c1.isIncreased());

    }

    @Test
    public void testContainerCloneIndepend() {
        CountDistinctContainer c1 = new CountDistinctContainer("Autreche", sinceWhenDate);
        // Initialize c1
        c1.add(new Date(), "test");

        CountDistinctContainer clone = (CountDistinctContainer) c1.clone();

        // make sure clone is independent of c1
        Assert.assertNotSame(c1, clone);
        Assert.assertNotSame(c1.getCountDistinctCounter(), clone.getCountDistinctCounter());

        // Mutate c1, make sure clone does not..
        c1.add(new Date(), "blubb");

        Assert.assertTrue(c1.getCount() > clone.getCount());
        Assert.assertTrue(c1.getCountDistinctCounter().getCountDistinct() > clone.getCountDistinctCounter()
            .getCountDistinct());

    }
}
