package eu.ferari.examples.countDistinct.common.test;

import org.junit.Assert;
import org.junit.Test;

import eu.ferari.core.DataTuple;
import eu.ferari.examples.countDistinct.common.ParseLineToTupleDetector;

public class ParseLineToTupleTest {

    @Test
    public void testProcess() {

        DataTuple tuple =
            new DataTuple(
                "user_000001\t2009-05-03T15:48:25Z\tba2f4f3b-0293-4bc8-bb94-2f73b5207343\tUnderworld\tdc394163-2b78-4b56-94e4-658597a29ef8\tBoy, Boy, Boy (Switch Remix)");
        DataTuple processedTuple = ParseLineToTupleDetector.process(tuple);
        Assert.assertNotNull(processedTuple);
        Assert.assertEquals("Underworld", processedTuple.getString(0));
    }

}
