package eu.ferari.examples.countDistinct.common;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.ferari.core.DataTuple;
import eu.ferari.core.utils.Helper;

/**
 * Converts a DataTuple with a last.fm tab delimited text line content to a
 * DataTuple, with fields artist_id and the complete record as LastFmDataRecord.
 * 
 */

public class ParseLineToTupleDetector {

    private static final char DELIMITER = '\t';
    private static final Logger LOG = LoggerFactory.getLogger(MusicDataRecord.class);

    public static DataTuple process(DataTuple input) {

        String inputString = input.getString(0);
        List<String> splitL = Helper.split(inputString, DELIMITER);

        if (splitL.size() == 6) {
            // Line is ok, construct the tuple
            MusicDataRecord dataRecord =
                new MusicDataRecord(splitL.get(0), splitL.get(1), splitL.get(2), splitL.get(3), splitL.get(4),
                    splitL.get(5));

            DataTuple output = new DataTuple(dataRecord.getArtname(), dataRecord);
            return output;

        } else {
            LOG.warn("Skipping invalid input line:" + inputString);
            return null;
        }

    }
}
