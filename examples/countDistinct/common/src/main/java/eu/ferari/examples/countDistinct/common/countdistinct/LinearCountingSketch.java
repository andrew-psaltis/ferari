/*
*** Copyright (c) 2014, Fraunhofer IAIS
*** All rights reserved.
 */
package eu.ferari.examples.countDistinct.common.countdistinct;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.ferari.core.utils.Check;

public class LinearCountingSketch {

    private static final Logger LOG = LoggerFactory.getLogger(LinearCountingSketch.class);

    private final Sketch sketch;
    private final int sketchsize;
    private final long a, b, p;

    public LinearCountingSketch(Sketch sketch, long a, long b, long p) {

        this.sketchsize = sketch.getSketchsize();
        this.sketch = sketch;

        this.a = a;
        this.b = b;
        this.p = p;

    }

    private String hashSHA(String s) { // SHA-2 hashing..

        StringBuffer sb = null;

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(s.getBytes());

            byte byteData[] = md.digest();

            sb = new StringBuffer();
            for (byte element : byteData) {
                String hex = Integer.toHexString(0xff & element);
                if (hex.length() == 1) {
                    sb.append('0');
                }
                sb.append(hex);
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return sb.toString();

    }

    private int hash(String id) {

        int hash = 0;

        try {

            BigInteger id_big;

            try {
                id_big = new BigInteger(hashSHA(id), 16);
            } catch (NumberFormatException e) {
                LOG.error("ERROR: not a valid representation of a BigInteger in the specified radix!");
                id_big = BigInteger.ZERO;
            }

            BigInteger a_big = BigInteger.valueOf(a);
            BigInteger b_big = BigInteger.valueOf(b);
            BigInteger p_big = BigInteger.valueOf(p);
            BigInteger sketchsize_big = BigInteger.valueOf(sketchsize);

            // hash(x) = ((a * x + b) % p) % sketchsize;
            hash = a_big.multiply(id_big).add(b_big).mod(p_big).mod(sketchsize_big).intValue();

        } catch (NullPointerException ne) {
            LOG.error("ERROR: input value was NULL!");
            return hash;
        }

        return hash;

    }

    public void update(String mac) { // update sketch with device-MAC -> set Bit at position = hash(mac)

        try {

            this.sketch.setBit(hash(mac));

        } catch (IndexOutOfBoundsException e) {
            LOG.error("ERROR: index is negative!");
        }

    }

    public long estimateDistinct() { // this method is used only in a JUnit test  ->  'use estimateDistinctFloat' !

        if (sketch != null) {

            float n = 0; // estimated number of distinct items
            int u = 0; // number of zeros in sketch
            double v = 0; // relative zero count
            int sketchsize = sketch.getSketchsize();

            u = sketchsize - this.sketch.cardinality();

            if (u == 0) {
                LOG.warn("ln(0) in estimation - discard this single count"); // important, if using mean !!
                return -1;
            }

            v = (double) u / sketchsize;

            n = (float) (-sketchsize * Math.log(v));

            return Math.round(n);

        } else {
            LOG.error("estimation error: sketch is NULL");
            return 0;
        }

    }

    public LinearCountingSketch copy() { // copy current sketcher..

        LinearCountingSketch sketcherCopy = new LinearCountingSketch(this.sketch.copy(), this.a, this.b, this.p);

        return sketcherCopy;

    }

    @Override
    public String toString() {
        return "Sketcher [sketch:" + this.sketch.toString() + ", sketchsize:" + this.sketchsize + ", a:" + this.a
                + ", b:" + this.b + ", p:" + this.p + "]";
    }

    public void mergeWith(LinearCountingSketch c) {
        Check.isEqual(a, c.a);
        Check.isEqual(b, c.b);
        Check.isEqual(p, c.p);
        Check.isEqual(sketchsize, c.sketchsize);

        this.sketch.mergeWidth(c.sketch);

    }

}
