/*
*** Copyright (c) 2014, Fraunhofer IAIS
*** All rights reserved.
 */
package eu.ferari.examples.countDistinct.common.countdistinct;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import eu.ferari.core.utils.Check;

/**
 * Count Distinct implemented with a Set.
 */

public class CountDistinctSetBased implements ICountDistinctCounter, Serializable {

    private static final long serialVersionUID = 1L;
    private final Set<String> elems;

    public CountDistinctSetBased() {
        this.elems = new HashSet<>();
    }

    protected CountDistinctSetBased(CountDistinctSetBased other) {
        // For set of Strings save without deep copy 
        elems = new HashSet<String>(other.elems);
    }

    /*
     * (non-Javadoc)
     * @see
     * eu.ferari.operations.countdistinct.ICountDistinctCounter#add(java.lang
     * .String)
     */
    @Override
    public void add(String id) {
        this.elems.add(id);

    }

    /*
     * (non-Javadoc)
     * @see
     * eu.ferari.operations.countdistinct.ICountDistinctCounter#getCountDistinct
     * ()
     */
    @Override
    public long getCountDistinct() {
        return this.elems.size();
    }

    /*
     * (non-Javadoc)
     * @see eu.ferari.operations.countdistinct.ICountDistinctCounter#reset()
     */
    @Override
    public void reset() {
        this.elems.clear();
    }

    /*
     * (non-Javadoc)
     * @see
     * eu.ferari.operations.countdistinct.ICountDistinctCounter#mergeWith(eu
     * .ferari.operations.countdistinct.ICountDistinctCounter)
     */
    @Override
    public void mergeWith(ICountDistinctCounter other) {
        Check.isTrue(other instanceof CountDistinctSetBased);

        this.elems.addAll(((CountDistinctSetBased) other).elems);

    }

    @Override
    public Object clone() {
        return new CountDistinctSetBased(this);
    }

}
