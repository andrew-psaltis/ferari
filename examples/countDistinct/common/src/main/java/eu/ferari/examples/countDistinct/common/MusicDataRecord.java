package eu.ferari.examples.countDistinct.common;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import eu.ferari.core.utils.CheckFailedException;

/**
 * Record for last.fm row.
 * <p>
 * Immutable data structure to hold the fields of a last.fm record.
 */
public class MusicDataRecord implements Serializable {

    private static final long serialVersionUID = 1L;
    private final String userId;
    private final Date timestamp;
    private final String artistId;
    private final String artistName;
    private final String trackId;
    private final String trackName;

    /**
     * Constructor for the immutable record.
     * 
     * @param userId the user's id
     * @param timestamp a timestamp in format declared at
     *            {@link countDistinctConstants.DATE_FORMAT}
     * @param artistId the artist's id
     * @param artistName the artist's name
     * @param trackId the track's id
     * @param trackName the track's name
     * @throws RuntimeException if wrong dateformat is used
     */
    public MusicDataRecord(String userId, String timestamp, String artistId, String artistName, String trackId,
                           String trackName) {
        this.userId = userId;

        try {
            this.timestamp = new SimpleDateFormat(countDistinctConstants.DATE_FORMAT).parse(timestamp);
        } catch (ParseException e) {
            throw new CheckFailedException("wrong date-format" + timestamp);
        }
        this.artistId = artistId;
        this.artistName = artistName;
        this.trackId = trackId;
        this.trackName = trackName;
    }

    public String getUserId() {
        return userId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getArtistId() {
        return artistId;
    }

    public String getArtname() {
        return artistName;
    }

    public String getTraid() {
        return trackId;
    }

    public String getTraname() {
        return trackName;
    }

}
