/*
*** Copyright (c) 2014, Fraunhofer IAIS
*** All rights reserved.
 */
package eu.ferari.examples.countDistinct.common.countdistinct;


import java.util.Date;


import eu.ferari.core.utils.Check;

/**
 * Container to hold countDistinct of one specific key.
 */
public class CountDistinctContainer implements Cloneable {

    private final ICountDistinctCounter countDistinct;
    private final Date sinceWhenDate;
    private Date lastUpdate; // time of last update
    private long count;
    private final String key;
    private boolean isIncreased;

    /**
     * Creates a count distinct container.
     * 
     * @param key of the thing to count
     * @param sinceWhenDate timestamp
     */
    public CountDistinctContainer(String key, Date sinceWhenDate) {
        this.key = key;
        this.isIncreased = false;
        this.countDistinct = CountDistinctCounterFactory.construct();
        this.count = 0L;
        this.sinceWhenDate = sinceWhenDate;
        this.lastUpdate = sinceWhenDate;
    }

    /**
     * Copy constructor to implement clone().
     * 
     * @param other the obj to base the new instance upon, not null
     */
    protected CountDistinctContainer(CountDistinctContainer other) {
        Check.notNull(other);
        this.key = other.key;
        this.count = other.count;
        this.isIncreased = other.isIncreased;
        this.sinceWhenDate = (Date) other.sinceWhenDate.clone();
        this.lastUpdate = (Date) other.lastUpdate.clone();
        this.countDistinct = (ICountDistinctCounter) other.countDistinct.clone();
    }

    public Date getSinceWhenDate() {
        return sinceWhenDate;
    }

    public ICountDistinctCounter getCountDistinctCounter() {
        return countDistinct;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getKey() {
        return key;
    }

    public boolean isIncreased() {
        return isIncreased;
    }

    public void add(Date date, String value) {
        Check.notNull(date);
        Check.notNull(value);

        if (lastUpdate.getTime() < date.getTime()) {
            lastUpdate = date;
        }

        this.countDistinct.add(value);
        long newCountDistinct = this.countDistinct.getCountDistinct();
        if (count < newCountDistinct) {
            isIncreased = true;
            this.count = newCountDistinct;
        } else {
            isIncreased = false;
        }
    }

    /**
     * Merge this container with the other one.
     * <p>
     * if keys of the containers match, merges the other containers count
     * distinct with this one. Also updates status of isIncreased accordingly.
     * 
     * @param other the other container, not null
     */
    public void mergeWith(CountDistinctContainer other) {
        Check.notNull(other);

        if (other.getKey().equals(key)) {
            // update timestamp if newer
            if (other.getLastUpdate().getTime() > lastUpdate.getTime()) {
                this.lastUpdate = other.getLastUpdate();
            }
            this.countDistinct.mergeWith(other.getCountDistinctCounter());
            long countDistinct = this.countDistinct.getCountDistinct();
            if (count < countDistinct) {
                isIncreased = true;
                count = countDistinct;
            } else {
                isIncreased = false;
            }
        } else {
            isIncreased = false;
        }
    }

    @Override
    public Object clone() {
        return new CountDistinctContainer(this);
    }

    @Override
    public String toString() {
        return key + ":" + count + " ";

    }
}
