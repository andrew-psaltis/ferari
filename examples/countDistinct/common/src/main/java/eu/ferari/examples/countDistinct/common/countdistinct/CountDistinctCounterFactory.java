/*
*** Copyright (c) 2014, Fraunhofer IAIS
*** All rights reserved.
 */
package eu.ferari.examples.countDistinct.common.countdistinct;

enum types {
    SKETCH, SET
}

public class CountDistinctCounterFactory {

    public static ICountDistinctCounter construct() {
        return new CountDistinctSketchBased();
    }

    public static ICountDistinctCounter construct(types type) {
        switch (type) {
        case SKETCH:
            return new CountDistinctSketchBased();
        case SET:
            return new CountDistinctSetBased();
        default:
            return new CountDistinctSketchBased();
        }
    }
}
