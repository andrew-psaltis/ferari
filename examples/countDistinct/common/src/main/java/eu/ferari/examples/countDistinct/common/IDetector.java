package eu.ferari.examples.countDistinct.common;

import eu.ferari.core.DataTuple;


public interface IDetector {
    DataTuple process(DataTuple dataTuple);
    void cleanup();
}
