/*
*** Copyright (c) 2014, Fraunhofer IAIS
*** All rights reserved.
 */
package eu.ferari.examples.countDistinct.common.countdistinct;

import java.io.Serializable;
import java.util.BitSet;

import eu.ferari.core.utils.Check;

public class Sketch implements Serializable {

    private static final long serialVersionUID = -754452134281626194L;

    private BitSet sketch;
    private int sketchsize;

    public Sketch(int sketchsize) {

        this.sketchsize = sketchsize;
        this.sketch = new BitSet();
        //this.sketch = new BitSet(sketchsize);

    }

    public void setBit(int index) {
        this.sketch.set(index);
    }

    public boolean getBit(int index) {
        return this.sketch.get(index);
    }

    public BitSet getSketch() {
        return sketch;
    }

    public void setSketch(BitSet sketch) {
        this.sketch = sketch;
    }

    public int getSketchsize() {
        return this.sketchsize;
    }

    public void setSketchsize(int sketchsize) {
        this.sketchsize = sketchsize;
    }

    public Sketch orSketch(Sketch sketch) {

        this.sketch.or(sketch.getSketch());

        Sketch newSketch = new Sketch(this.sketchsize);
        newSketch.setSketch(this.sketch);

        return newSketch;

    }

    public void clear() {

        this.sketch.clear();
        this.sketchsize = 0;

    }

    public Sketch copy() {

        Sketch sketchCopy = new Sketch(this.sketchsize);
        sketchCopy.setSketch((BitSet) this.sketch.clone());

        return sketchCopy;

    }

    @Override
    public String toString() {
        return "Sketch [sketchsize:" + this.sketchsize + ", " + this.sketch.toString() + "]";
    }

    public int cardinality() {
        return this.sketch.cardinality();
    }

    public void mergeWidth(Sketch s) {
        Check.isEqual(sketchsize, s.sketchsize);
        this.sketch.or(s.sketch);

    }

}
