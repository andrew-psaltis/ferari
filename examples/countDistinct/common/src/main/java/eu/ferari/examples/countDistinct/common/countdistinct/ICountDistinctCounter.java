/*
*** Copyright (c) 2014, Fraunhofer IAIS
*** All rights reserved.
 */
package eu.ferari.examples.countDistinct.common.countdistinct;

public interface ICountDistinctCounter extends Cloneable {
    public void add(String id);

    public long getCountDistinct();

    public void mergeWith(ICountDistinctCounter counter);

    public void reset();

    public Object clone();
}
