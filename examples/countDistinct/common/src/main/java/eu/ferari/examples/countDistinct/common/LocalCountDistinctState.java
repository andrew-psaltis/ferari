package eu.ferari.examples.countDistinct.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ILocalState;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.countDistinct.common.countdistinct.CountDistinctContainer;

/**
 * Local Count Distinct model.
 * <p>
 * ...
 */

public class LocalCountDistinctState implements ILocalState {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(LocalCountDistinctState.class);
    private final Map<String, CountDistinctContainer> counts;
    private ISend sendToCoordinator;
    private int numTuplesSeen = 0;
    private Date sinceWhenDate;

    public LocalCountDistinctState() {
        this.counts = new HashMap<String, CountDistinctContainer>();
    }

    @Override
    public void update(DataTuple input) {

        MusicDataRecord record = (MusicDataRecord) input.getValue(0);
        ++numTuplesSeen;

        Date recordTime = record.getTimestamp();
        if (sinceWhenDate == null) {
            sinceWhenDate = recordTime;
        }

        // do count distinct
        if (recordTime.getTime() >= sinceWhenDate.getTime()) {
            String userId = record.getUserId();
            // Add the user for the current artistId
            addUserToCounter(recordTime, userId, record.getArtname());
            // And also for the set of all users in the entire dataset
            addUserToCounter(recordTime, userId, countDistinctConstants.USER_COUNTDISTINCT);
        }

        // Log some progress..
        if (numTuplesSeen % 10000 == 0) {
            SimpleDateFormat format = new SimpleDateFormat(countDistinctConstants.DATE_FORMAT);
            LOG.info("Number of tuples seen so far:" + numTuplesSeen + ":" + format.format(recordTime));
        }
    }

    private void addUserToCounter(Date recordTime, String userId, String keyForCounter) {

        CountDistinctContainer counter = counts.get(keyForCounter);

        if (counter == null) {
            counter = new CountDistinctContainer(keyForCounter, this.sinceWhenDate);
            counts.put(keyForCounter, counter);
        }

        counter.add(recordTime, userId);

        // send update to global model if count distinct was increased
        if (counter.isIncreased()) {
            DataTuple data = new DataTuple(counter);
            if (sendToCoordinator != null) {
                sendToCoordinator.signal(data);
            }
        }
    }

    @Override
    public void handleFromCoordinator(DataTuple data) {
        // reset state and set new reference time
        Date sinceWhenDate = (Date) data.getValue(0);
        this.sinceWhenDate = sinceWhenDate;
        counts.clear();

        // Log the re-init
        SimpleDateFormat format = new SimpleDateFormat(countDistinctConstants.DATE_FORMAT);
        LOG.info("Local Model reset at :"
                + format.format(sinceWhenDate)
                + ":"
                + "=======================================================================================================");
    }

    @Override
    public void setSender(ISend sendToCoordinator) {
        this.sendToCoordinator = sendToCoordinator;
    }
}
