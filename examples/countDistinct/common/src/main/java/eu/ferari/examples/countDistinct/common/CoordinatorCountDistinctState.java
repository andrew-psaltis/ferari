package eu.ferari.examples.countDistinct.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.ferari.core.DataTuple;
import eu.ferari.core.interfaces.ICoordinatorState;
import eu.ferari.core.interfaces.ISend;
import eu.ferari.examples.countDistinct.common.countdistinct.CountDistinctContainer;

/**
 * Get counts from local models
 */

public class CoordinatorCountDistinctState implements ICoordinatorState {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(CoordinatorCountDistinctState.class);
    private final Map<String, CountDistinctContainer> counts;
    private ISend sendToLocal;
    private Date sinceWhenDate;

    public CoordinatorCountDistinctState() {
        this.counts = new HashMap<String, CountDistinctContainer>();
    }

    @Override
    public void update(DataTuple input) {
        CountDistinctContainer countDistinct = (CountDistinctContainer) input.getValue(0);
        updateModel(countDistinct);

    }

    private void updateModel(CountDistinctContainer countDistinct) {
        SimpleDateFormat format = new SimpleDateFormat(countDistinctConstants.DATE_FORMAT);

        Boolean isnew = false;
        CountDistinctContainer globalCounter = counts.get(countDistinct.getKey());

        if (sinceWhenDate == null) {
            sinceWhenDate = countDistinct.getSinceWhenDate();
        }
        if (countDistinct.getSinceWhenDate().getTime() >= sinceWhenDate.getTime()) { //drop otherwise

            if (globalCounter == null) {
                globalCounter = (CountDistinctContainer) countDistinct.clone();
                counts.put(globalCounter.getKey(), globalCounter);
                isnew = true;

            } else {
                globalCounter.mergeWith(countDistinct);
            }

            long count = globalCounter.getCount();

            // notifiy if number of users is increased
            if (isnew || globalCounter.isIncreased()) {

                CountDistinctContainer allUsers = counts.get(countDistinctConstants.USER_COUNTDISTINCT);
                long allUsersCount = 1;
                if (allUsers != null) {
                    allUsersCount = allUsers.getCount();
                }
                count = globalCounter.getCount();

                LOG.info("Global Model update:" + format.format(globalCounter.getLastUpdate()) + ":"
                        + globalCounter.getKey() + ":" + count + ":" + count * 1.0 / allUsersCount + ":of "
                        + allUsersCount + ":since " + format.format(sinceWhenDate));

                if (globalCounter.getKey().equals(countDistinctConstants.USER_COUNTDISTINCT)
                        && globalCounter.getCount() >= countDistinctConstants.MAX_REACH) {
                    sinceWhenDate = globalCounter.getLastUpdate();
                    counts.clear();

                    sendToLocal.signal(new DataTuple(sinceWhenDate));

                    LOG.info("Global Model reset at :"
                            + format.format(globalCounter.getLastUpdate())
                            + ":"
                            + globalCounter.getKey()
                            + ":"
                            + count
                            + ":"
                            + count
                            * 1.0
                            / allUsersCount
                            + ":of "
                            + allUsersCount
                            + ":since "
                            + format.format(sinceWhenDate)
                            + "=======================================================================================================");

                }
            }
        }
    }

    @Override
    public void setSender(ISend sendToLocal) {
        this.sendToLocal = sendToLocal;

    }

}
