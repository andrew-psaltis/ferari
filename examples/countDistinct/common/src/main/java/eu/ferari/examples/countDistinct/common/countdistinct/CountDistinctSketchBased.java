/*
*** Copyright (c) 2014, Fraunhofer IAIS
*** All rights reserved.
 */
package eu.ferari.examples.countDistinct.common.countdistinct;

import java.io.Serializable;

import eu.ferari.core.utils.Check;

/**
 * Count Distinct implemented with a Sketch.
 */

public class CountDistinctSketchBased implements ICountDistinctCounter, Serializable, Cloneable {

    private static final long serialVersionUID = 1L;
    private LinearCountingSketch sketchBuilder;
    private final int n;
    private final long a;
    private final long b;
    private final long p;

    public CountDistinctSketchBased(int n, long a, long b, long p) {
        this.n = n;
        this.a = a;
        this.b = b;
        this.p = p;
        this.reset();

    }

    public CountDistinctSketchBased() {
        this.n = 1000;
        this.a = 1237976184732196947L;
        this.b = 4774147450029365205L;
        this.p = 9111974411021516999L;

        this.reset();

    }

    @Override
    public void add(String id) {
        this.sketchBuilder.update(id);

    }

    @Override
    public long getCountDistinct() {
        return this.sketchBuilder.estimateDistinct();
    }

    @Override
    public void reset() {
        Sketch sketch = new Sketch(n);
        this.sketchBuilder = new LinearCountingSketch(sketch, a, b, p);

    }

    @Override
    public void mergeWith(ICountDistinctCounter counter) {
        Check.isInstanceOf(counter, CountDistinctSketchBased.class);
        CountDistinctSketchBased c = (CountDistinctSketchBased) counter;
        Check.isEqual(a, c.a);
        Check.isEqual(b, c.b);
        Check.isEqual(p, c.p);
        Check.isEqual(n, c.n);

        this.sketchBuilder.mergeWith(c.sketchBuilder);

    }

    @Override
    public Object clone() {
        CountDistinctSketchBased clone;
        try {
            clone = (CountDistinctSketchBased) super.clone();
            clone.sketchBuilder = this.sketchBuilder.copy();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }

        return clone;
    }

}
