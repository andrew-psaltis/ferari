![FERARI](http://www.ferari-project.eu/wp-content/uploads/2014/02/logo_new1.png)
###Project public repository FERARI EU project Grant No 619491.###
[![wercker status](https://app.wercker.com/status/482f47cbc752fe3992d1c41bd4ad346a/m/master "wercker status")](https://app.wercker.com/project/bykey/482f47cbc752fe3992d1c41bd4ad346a)

About
=====
The goal of __FERARI__ is to provide framework for distributed computation of streams while minimizing communication.
This is achieved by maintaining local models that send messages only when they are violated.

### Core ###
Module __core__ provides general interfaces that are independent of __runtime adaption__.
The basic components are __local state__ which accepts messages and may contact a __coordinator__ if needed.
The _coordinator_ in turn can send updated model information to the instances of _local state_.
Of course it is completely feasible to contact the _coordinator_ after each message to the _local models_.
However this is not recommended as it will become a bottle neck when processing large amounts of data in a distributed fashion.
For further information please read the [Overview Slides](http://sbothe-iais.bitbucket.org/ferari/FerariArchitecture.pdf)

### Runtime Adaptions ###
Every pair of _local/global_ models expressed using the interfaces defined in _core_ should be able to run on different _runtime adaptions_.
Please contribute new adapters.

### CEP on Storm ###
[Proton on Storm](https://github.com/ishkin/Proton/tree/master/IBM%20Proactive%20Technology%20Online%20on%20STORM)

Repository
===========
Maven is used for building the project and maintaining dependencies.
There are modules for _core_, _runtime adaptions_ and _examples_.
The _runtime adaptions_  module has a submodule for each adaption.
_Examples_ will have a submodule for their implementations of _local_ and _global_ states as well as submodules deploying it to different _runtime adaptions_.


Running an example
==================
After installing all necessary dependencies via _Maven_, you can run the example found in ```examples/countDistinct/storm/src/main/java/CountDistinctTopology```.
However you have to change the working directory to ```examples/countDistinct/common/``` as a sample input is found in the ```data``` folder of that directory.
When running the example, the last few messages should contain:
```INFO  eu.ferari.examples.countDistinct.common.CoordinatorCountDistinctState - Global Model update```
Do not forget to kill the process, as it awaits additional input data.



License
============

   Copyright 2014 FERARI Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.