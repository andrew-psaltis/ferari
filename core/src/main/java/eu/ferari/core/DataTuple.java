package eu.ferari.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Implements an immutable tuple data structure.
 */
public final class DataTuple implements Serializable {

    private static final long serialVersionUID = 1L;

    private final List<Object> values;

    public DataTuple(Object... objects) {
        this.values = new ArrayList<Object>(Arrays.asList(objects));
    }
    
    public DataTuple(List<Object> objects){
    	this.values = new ArrayList<Object>(objects);
    }
    
    public void add(Object... objects) {
        for (Object object : objects) {
            this.values.add(object);
        }
    }

    public Object getValue(int index) {
        return this.values.get(index);
    }

    public String getString(int index) {
        return (String) this.values.get(index);
    }

    public Integer getInteger(int index) {
        return (Integer) this.values.get(index);
    }

    public Long getLong(int index) {
        return (Long) this.values.get(index);
    }

    public Boolean getBoolean(int index) {
        return (Boolean) this.values.get(index);
    }

    public Short getShort(int index) {
        return (Short) this.values.get(index);
    }

    public Byte getByte(int index) {
        return (Byte) this.values.get(index);
    }

    public Double getDouble(int index) {
        return (Double) this.values.get(index);
    }

    public Float getFloat(int index) {
        return (Float) this.values.get(index);
    }

    public byte[] getBinary(int index) {
        return (byte[]) this.values.get(index);
    }

    public List<Object> asList() {
        return Collections.unmodifiableList(this.values);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.values == null ? 0 : this.values.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        DataTuple other = (DataTuple) obj;
        if (this.values == null) {
            if (other.values != null) {
                return false;
            }
        } else if (!this.values.equals(other.values)) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Tuple [values=" + this.values + "]";
    }
}
