package eu.ferari.core.interfaces;

import eu.ferari.core.DataTuple;

public interface ISend {

    public void signal(DataTuple data);

}
