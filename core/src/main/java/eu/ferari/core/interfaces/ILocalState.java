package eu.ferari.core.interfaces;

import java.io.Serializable;

import eu.ferari.core.DataTuple;

public interface ILocalState extends ICoordinatorState, Serializable {
    public void handleFromCoordinator(DataTuple data);
}
