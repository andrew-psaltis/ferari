#!/bin/bash
redis-server &
/usr/share/zookeeper/bin/zkServer.sh start
/usr/lib/kafka/bin/kafka-server-start.sh -daemon /usr/lib/kafka/config/server.properties
/usr/lib/storm/bin/storm nimbus &
/usr/lib/storm/bin/storm supervisor &
/usr/lib/storm/bin/storm ui
